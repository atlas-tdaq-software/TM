#ifndef TM_CLIENT_H
#define TM_CLIENT_H

#include <string>
#include <vector>
#include <future>
#include <atomic>
#include <memory>
#include <set>

#include <tbb/concurrent_unordered_map.h> 

#include <config/Configuration.h>
#include <config/DalObject.h>
// core DAL
#include <dal/Partition.h>
#include <dal/TestableObject.h>
#include <dal/Computer.h>
#include <dal/ComputerSet.h>
// TM DAL
#include <TestManager/Test.h>

#include <TM/TestResult.h>
#include <TM/ResultsCache.h>
#include <TM/Handle.h>
#include <TM/TimeoutHandler.h>
#include <TM/ThreadPool.h>

namespace daq {
namespace tm {

const char TDAQ_TMGR_FAILURES_THRSHLD_ENVVAR[] = "TDAQ_TMGR_FAILURES_THRSHLD" ;
const size_t default_failures_threshold { 2 } ; // how many comm failures per host tolerated before the host is execluded from the pool

class TestHandle ; // solve cyclic deps

/**
 * \class daq::tm::Client
 *
 * \brief Entry point to TM library and provider of the main functionality: testComponetn
 * 
 * Not constructable, instance returned from Client:test()
 * Upon destruction, stops all the tests
 */
class Client: public TimeoutHandler, public ResultsCache {

public:

    /** Constructor
     *  \throw NoTestHosts if no hosts found for launching tests in Configuration
     */
    Client (Configuration& config, const daq::core::Partition& partition) ;
    ~Client() ;    
    
     /**
      * Default callback, used in TestComponent when no callback is required
      */
    static void empty_callback(const ComponentResult&) ;

    /**
     *  \brief asynchronously start testing a component (TestableObject)
     * 
     *  returns to client non-owning pointer to Handle (and Tester) in order to avoid client to syncronize deletion of Handles and TM Client object
     * 
     *  \param comp the TestableObject we want to test
     *  \param callback the function to be called once testing is finished (provide Client::empty_callback when not needed)
     *  \param scope a string indicating the scope (selection of tests out of all available for the component)
     *  \param level a int indicating the level of testing (selection of tests out of all available for the component)
     *  \return a TestHandle an object to control the ongoing tests and to get the result syncronously
     */
    std::weak_ptr<TestHandle>
    testComponent(
                    const daq::core::TestableObject& comp,
                    const std::string test = "", // default - all tests
                    TestResultCallback callback = empty_callback,
                    std::string scope = "any",
                    int level = 255); 

    /**
     *  \brief reset the validity of test results for a component
     *  \param comp the TestableObject
     */
    void reset(const daq::core::TestableObject& comp);

    /**
     *  \brief get a list of tests for a testable component
     *  \param comp a Component for which we want to retrieve tests
     *  \param scope a string indicating the category of tests we are interested in (default = any)
     *  \param level a int indicating the level of tests we are interested in (default = all)
     *  \return a vector of Tests defined for this TO (these are const DAL objects, no need to deallocate)
     * 
     *  \see daq::tm::Tests
     */
    std::vector<const daq::tm::Test*> getTestsForComponent( const daq::core::TestableObject& comp,
                                                            std::string scope = "any",
                                                            int level = 255) noexcept ;

    // get/set verbosity of test execution ("-v" is added to cmdline params)
    bool isVerbose() const { return m_verbose ; }
    void setVerbose( bool verbose ) { m_verbose = verbose ; }
    bool isDiscardLogs() const { return m_discard_output ; }
    void setDiscardLogs(bool discard) { m_discard_output = discard ; }
    size_t getFailuresThreshold() { return m_failures_threshold ; }
    
    // handle #this.UID syntax
    // \throw daq::tm::InconsistentData
    void substituteParam(const DalObject& obj, std::string& params, bool forceObj = false ) const ;

    // globally accessible thread pool
    // static ThreadPool& getThreadPool() { return s_thread_pool ; } 

    // accessors needed everywhere
    Configuration& getConfig() const { return m_config ; } 
    const daq::core::Partition& getPartition() const { return m_partition ; } 

    // get random host from test host cluster (used in TestTask launcher)
    const std::optional<const daq::core::Computer*> get_random_host() const ;

    void schedule(std::function<void()>&& f) { m_thread_pool->schedule(f) ; } ; 

private:

    Configuration& m_config ;                       // reference to Configuration where all Tests and policies are defined
    const daq::core::Partition& m_partition ;       // current DAQ partition
    std::vector<const daq::core::Computer*> m_test_hosts ;  // set of hosts that can be used to launch tests
    size_t m_failures_threshold { default_failures_threshold } ;

    tbb::concurrent_unordered_map<std::string, std::atomic<size_t>> m_bad_counters ; // counter of error per test host

    bool m_verbose = true ;
    bool m_discard_output = true ;

    std::set<const daq::tm::Test*> m_all_tests ; // collection of all defined tests
    template<class T> void add_tests(const T* container) ;

    // retrieves from Configuration all test hosts defined for the partition
    void get_test_hosts(const daq::core::ComputerSet& cs) ; 

    ThreadPool* m_thread_pool ;
    tbb::concurrent_unordered_map<std::string, std::shared_ptr<TestHandle>> m_handles ; // collection of Handles of all launched tests
} ;


}}

#endif
