#ifndef TM_CTESTER_H
#define TM_CTESTER_H

#include <vector>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <map>
#include <atomic>

//#include <tbb/concurrent_queue.h>

#include <dal/TestableObject.h>
#include <TestManager/Test.h>

#include <TM/TestResult.h>

namespace daq {
namespace tm {

class TestTask ; // break cyclic header dependency
class Client ;

/** \class ComponentTester 
 *  \brief organizes testing of a particular Component
 * 
 * does not launch individual processes, but creates and launches TestTasks - one per Test defined for this component
 * holds a promise to ComponentResult, corresponding future is given to the caller
 */
class ComponentTester: public std::enable_shared_from_this<ComponentTester>
{
friend class Client ;       // the only place ComponentTester is created
friend class TestHandle ;   // need access to m_result
friend class TimeoutHandler ; 

public:

const daq::core::TestableObject& getComponent() const noexcept { return m_component ; } ; // accessor
Client& getClient() const noexcept { return m_client ; } ; // accessor

/**
 * called (from PMG callback TestTask::updateStatus or from launchTask) when a Tasks completed -> need to evaluate next steps
*/
void  taskCompleted() ;    

~ComponentTester() ;

private:

/**
 * \brief retreives Tests for this component, creates corresponding TestTasks and start launching them
 * 
 * private constructor, accessible from friend Client class where the tester is created and returned to the client
*/
ComponentTester(
    const std::vector<const daq::tm::Test*>& tests,
    const daq::core::TestableObject& object, 
    const std::string& test,   
    Client& client,    
    TestResultCallback& callback ) ;          

/**
 * \brief called (from PMG callback TestTask::updateStatus) when all Tasks completed, sets the future and calls user callback
 * 
 * \param ex ers::Issue that will be injected in TestResult
*/
void fulfillResult(const ers::Issue* ex = nullptr) noexcept ;     

// void  launchTask(TestTask* task) noexcept ;  // this function is submitted to the threadpool, launches next task

// called a) upon construction b) upon completion of a TestTask, to check if a depednent Test is to be launched
void launchAllReadyTasks() noexcept ;

// terminates all tests in progress
void stopTests() noexcept ;

/**
 * 
 * \throw BadTestConfiguration
*/
void checkDependencies() ; 

/** auxillary function for resolving circular dependencies
 * \throw CircularTestDependency
*/
void visitNodes(std::unordered_set<std::string>& visited, std::vector<const daq::tm::Test*> children) ;

const daq::core::TestableObject&    m_component ;  // ref to a tested object (immutable)
Client&                             m_client ;     // ref to a parent tm::Client
std::map<std::string, std::shared_ptr<TestTask>>   m_tasks ;     // collection of all Tasks
std::promise<ComponentResult>       m_result ; // promise of the result, future is owned by the caller Client:testComponent()
TestResultCallback                  m_callback ; // users callback
std::mutex                          m_sync_mutex ; // syncronize concurrent callbacks from multiple tasks
std::atomic_bool                    m_state ; // completion state
} ;

}}

#endif
