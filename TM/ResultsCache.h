#ifndef TM_RESCACHE_H
#define TM_RESCACHE_H

#include <string>
#include <vector>
#include <tuple>
#include <optional>

#include <tbb/concurrent_hash_map.h>

#include <TestManager/Test.h>
#include <TM/TestResult.h>

namespace daq::tm {

/**
 * \class ResultsCache
 *
 * \brief Keeps a map of @see TestResult per component ID. Used when launching tests in @see TestTask to get immediate cached result. @see Client inherits from this class.
 * 
 */
class ResultsCache {

public:

    ResultsCache () {} ;
    ~ResultsCache() { m_cache.clear() ; } ;    

    // store the TestResult per Compoenent and Test, key = objectId@testId
    void cacheResult(const std::string& comp_id, const std::string& test_id, const TestResult& res) ;
 
    // retreives the cached result or nothing
    std::optional<TestResult> getCachedResult(const std::string& comp_id, const daq::tm::Test& test) ;

    // reset 
    void resetCachedResult(const std::string& comp_id, const std::vector<const daq::tm::Test*>& tests) ;

private:

    std::string build_id(const std::string& comp_id, const std::string& test_id) { return comp_id + "@" + test_id ; } ;

    // the cache, key: objectId@testId, value: @see TestResult
    tbb::concurrent_hash_map<std::string, TestResult> m_cache ;
} ;

}
#endif