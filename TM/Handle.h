#ifndef TM_HANDLE_H
#define TM_HANDLE_H

#include <future>
#include <memory>

#include <TM/TestResult.h>
//#include <TM/ComponentTester.h> // do not expose the implementation to clients, allowing easy patching

namespace daq {
namespace tm {

class ComponentTester ;

/**
 * \class TestHandle
 *
 * \brief Non-owing pointer of TestHandle object returned to the user, allowing to control (stop) the ongoing test and to get test result and output.
 * 
 * Keeps a owning reference to a ComponentTester which is handling all the tests, and future<ComponentResult> which is eventually fulfilled in the Tester.
 * Constrcuted in Client::testComponent, a shared_ptr is kept in Client and destroyed in ~Client.
 * weak_ptr to it is returned to user from Client:test(), allowing to stop the tests and to get the result.
 * Upon destruction, stops all the tests. 
 * 
 * This class is exposed to clients.
 */
class TestHandle {
friend class Client ; // TestHandle constructed in Client start test method

public:
    ~TestHandle() ; 

    /**
     *  Stops all ongoing tests, waits for PMG callbacks, fullfills the result
    */
    void stop()  ;   

    /**
     * Returns \see ComponentResult from the future. Will block untill 
    */
    ComponentResult getResult() { return m_result.get() ; } ; // blocks until result is fulfilled (from ComponentTester), throw ers::Issue
// TODO get Runtime output?
// TODO return also the future, so the client can call oyhrer methods like wait_for ?

private:
    // private constrcutor, used by Client only. Takes a pointer to the ComponentTester.
    TestHandle( std::shared_ptr<ComponentTester> tester ) ; 
 
    std::shared_ptr<ComponentTester> m_tester ; // keeps reference to a ComponentTester in users' scope
    std::future<ComponentResult> m_result ;     // future to be fulfilled in ComponentTester
} ;

}}

#endif
