#ifndef TM_TESTRESULT_H
#define TM_TESTRESULT_H

#include <cstdint>
#include <string>
#include <vector>
#include <functional>
#include <chrono>
#include <memory>

#include <ers/Issue.h>

#include <tmgr/tmresult.h> // defines enum, daq::tmgr::TestResult { Passed=0, Undefined=182, Failed=183, Unresolved, Untested, Unsupported }

#include <TestManager/TestFailureAction.h>

namespace daq {
namespace tm {

/**
* \struct TestResult
* \brief result of a single Test for a Component (combined results of several dal::Executables, or output from CORBA test)
* \namespace daq::tm
*/
struct TestResult {
        uint32_t return_code ; // return code of the last Executables
        std::string test_id ; // some meaningful single test name e.g. Test ID from DB
        std::string stdout ;  // combined std out/err from all Executables
        std::string stderr ;
        std::vector<const daq::tm::TestFailureAction*> actions ; // follow up actions @see TestFailureAction
        std::string diagnosis ; // derived from Failures, if any
        std::chrono::microseconds duration ; // how long it took to execute the test
        std::chrono::system_clock::time_point test_time ;       // timepoint when the result was retrieved
        std::vector<std::shared_ptr<ers::Issue>> issues ;       // filled in case ComponentResult.result==Untested, i.e. there was an issue when launching a test
                                                                // also raised in TestHandle::getResult() 
};

/**
 *  \struct ComponentResult
 *  \brief This structure describes the outcome of a set of all tests defined and executed for a component,
 * contains single combined result and a vector of individual tests @see TestResult.
 *  \namespace daq::tm
 */
struct ComponentResult {
        tmgr::TestResult result; // enum { Passed, Undefined, Failed, Unresolved, Untested, Unsupported }
        std::vector<daq::tm::TestResult> test_results;    // detailed results of single Tests
        std::string component_id ; // classid\@uid
};

// typedef for result callback, function getting @see ComponentResult
using TestResultCallback = std::function<void(const ComponentResult&)> ;

}}
#endif
