#ifndef TM_EXCEPTIONS_H
#define TM_EXCEPTIONS_H

#include "ers/ers.h"

namespace daq {

/**
 * \class daq::tm::Exception
 *
 * \brief  This is a base class for all test manager exceptions.
 */
ERS_DECLARE_ISSUE(tm, Exception, ERS_EMPTY, ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(tm,
                       NotInitialized,
                       tm::Exception,
                       "The service cannot be used because it has not been initialized yet",
                       ERS_EMPTY,
                       ERS_EMPTY)

ERS_DECLARE_ISSUE_BASE(tm,
                       BadDependency,
                       tm::Exception,
                       "Bad test dependency detected for " << test,
                       ERS_EMPTY,
                       ((std::string) test))

ERS_DECLARE_ISSUE_BASE(tm,
                       BadTestConfiguration,
                       tm::Exception,
                       "Test " << test1 << " required from dependency of test " << test2 << " is not defined for object " << object,
                       ERS_EMPTY,
                       ((std::string) test1) ((std::string) test2) ((std::string) object ))

ERS_DECLARE_ISSUE_BASE(tm,
                       CircularTestDependency,
                       tm::Exception,
                       "Test " << test << " has a circular dependency" ,
                       ERS_EMPTY,
                       ((std::string) test))

ERS_DECLARE_ISSUE_BASE(tm,
                       DependencyCycle,
                       tm::Exception,
                       "Dependency detected for " << test,
                       ERS_EMPTY,
                       ((std::string) test))

ERS_DECLARE_ISSUE_BASE(tm,
                       ExecStartTimeout,
                       tm::Exception,
                       "Executable " << test << " did not start on time. Abandon this test.",
                       ERS_EMPTY,
                       ((std::string) test))

ERS_DECLARE_ISSUE_BASE(tm,
                       InconsistentData,
                       tm::Exception,
                       txt,
                       ERS_EMPTY,
                       ((std::string) txt) ((std::string) objId))

ERS_DECLARE_ISSUE_BASE(tm,
                       NoTestHosts,
                       tm::Exception,
                       "Partition " << partition << " doesn't contain any good host to run tests on (TestHosts, DefaultHost are empty)!",
                       ERS_EMPTY,
                       ((std::string) partition))

ERS_DECLARE_ISSUE_BASE(tm,
                       BadTag,
                       tm::Exception,
                       "No tag found in partition " << part << " to execute Test " << test<< " on host " << host,
                       ERS_EMPTY,
                       ((std::string) part) ((std::string) test) ((std::string) host))

ERS_DECLARE_ISSUE_BASE(tm,
                       FailedToLaunch,
                       tm::Exception,
                       "Failed to launch test " << test << " on host " << host,
                       ERS_EMPTY,
                       ((std::string) test) ((std::string) host))

ERS_DECLARE_ISSUE_BASE(tm,
                       BadTestHost,
                       tm::Exception,
                       "Failed to launch test on host " << host << ". Will re-try, consider inspecting the host or remove it from TestHosts ComputerSet",
                       ERS_EMPTY,
                       ((std::string) host))

}

#endif

