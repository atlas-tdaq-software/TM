#ifndef TM_TTASK_H
#define TM_TTASK_H

#include <stdint.h>
#include <tuple> 
#include <variant> 
#include <atomic>
#include <mutex>
#include <chrono>
#include <memory>
#include <string>
#include <unordered_map> 

#include <ProcessManager/Process.h>
#include <ProcessManager/ProcessDescription.h>

#include <config/Configuration.h>

// TM DAL
#include <TestManager/ExecutableTest.h>
#include <TestManager/Test4CORBAServant.h>
#include <TestManager/Executable.h>
#include <TestManager/Test.h>

#include <TM/ComponentTester.h>
#include <TM/TestResult.h>

// using process_sp_t = std::atomic<std::shared_ptr<daq::pmg::Process>> ;
// C++20. Unfortunately there is no semantic of Process* (like with shared_ptr), so the code accessing this object needs to be changed
using process_sp_t = std::shared_ptr<daq::pmg::Process> ;
using exec_test_t = std::tuple<const daq::tm::Executable*, process_sp_t, std::atomic_bool> ; // holder for Process tests, bool=true when process scheduled
using corba_test_t = const daq::tm::Test4CORBAServant* ;
using tests_t = std::unordered_map<std::string, exec_test_t> ; // map of test IDs to executable test tuple tructure
// using tests_t = tbb::concurrent_unordered_map<std::string, exec_test_t> ;

namespace daq {
namespace tm {

/**
 * \class TestTask
 * \brief A central class, tracks and synchronizes (ordered or parallel) execution of all test Processes (Executable) for a particular Test of a TestableObject
 * 
 */
class TestTask: public std::enable_shared_from_this<TestTask> {

public:

enum Status { Ready, Notready, Inprogress, Completed, Killed }  ; // status of the task

/**
 * constructs data strcutures
 * only creates and fills in m_tests map, the real start of test processes is done from launchExecTest
 */
TestTask(const daq::tm::Test& test, const std::string& objuid, Configuration& config) ; // constructs data strcutures

~TestTask() ;   // unlink processes

void launch(std::shared_ptr<ComponentTester> tester) noexcept ;      // start executing of test Execs
void complete(uint32_t res) noexcept ;         // finish the task: fill m_result 

/**
 * \brief terminates all running tests (used in timeout handler)
 * \returns false if there were issues in stopping the RUNNING processes, so cleanup may be needed
 */ 
void stop(bool complete = false) noexcept ;

void launchExecTest( const std::string& exectestid, std::weak_ptr<TestTask> wptask ) ;                  // launches a single Executable process asyncronously in thread pool
void executeCORBATest(std::weak_ptr<TestTask> taskwp) ;    // executes and syncronously gets result of a local CORBA test

bool isCompleted()  noexcept        { std::lock_guard lock(m_sync_mutex) ; return m_status == Completed ; } ;
bool isReady()      noexcept        { std::lock_guard lock(m_sync_mutex) ; return m_status == Ready ; } ;
void setNotReady()  noexcept        { std::lock_guard lock(m_sync_mutex) ; m_status = Notready ; } ;
void setReady()     noexcept        { std::lock_guard lock(m_sync_mutex) ; m_status = Ready ; } ;
Status getStatus()  noexcept        { std::lock_guard lock(m_sync_mutex) ; return m_status ; }
const daq::tm::Test& getTest() const noexcept { return m_test ; } ;
const TestResult& getResult() const noexcept { return m_result ; } ;
const daq::tmgr::TestResult getTmResult() const noexcept { return (daq::tmgr::TestResult)(m_result.return_code) ; } ;

/**
 * \brief periodically check the elapsed time and return true if timeout reached and it needs to be stopped
 * \returns true if timeout occured and tests processes are to be stopped (from TimeoutHandler thread), false otherwise
 */
bool checkTimeout() ; // periodically check the elapsed time and stop if timeout reached

// called from Corba threads
// daq::pmg::CallBackFncPtr processCallback ;
// just adds real call to a threadpool
void processCallbackAsync(process_sp_t process, std::weak_ptr<TestTask> taskwp, void*) ;

private:

// real callback, called from threadpool
void processCallback(process_sp_t process, std::weak_ptr<TestTask> wptask) ;

// called from processCallback, in turn calls corresponding ComponentTester code to launch next Task if needed
void launchRemainingExecs() noexcept ;   

std::weak_ptr<ComponentTester> m_tester ;     // for substitution of parameters 
const daq::tm::Test& m_test ;   // for access to timeout, validity, repeat..
TestResult m_result ;           // combined result of all Executables or CORBA test
Status m_status ;
std::chrono::system_clock::time_point m_start_time ;
std::chrono::system_clock::time_point m_end_time ;
std::mutex m_sync_mutex ; // controls access to m_result

tests_t m_exec_tests ;      // map of IDs (of Test Executable) to Processes 
corba_test_t m_corba_test ; // only one possible CORBA test
} ;

using TestTaskSP = std::shared_ptr<TestTask> ;

}}

#endif
