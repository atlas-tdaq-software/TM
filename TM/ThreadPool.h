/**
 *  \file ThreadPool.h
 *  Implementation of a simple thread pool class.
 *  \anchor Kazarov A.
 *  \date 12.11.2012
 *  \copyright 2012-2023 CERN for the benefit of the ATLAS Collaboration. All rights reserved.
 *
 */

#ifndef TM_TPOOL_H
#define TM_TPOOL_H

#include <stdlib.h>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <functional>

// a class template for an unbounded first-in-first-out data structure that permits multiple threads to concurrently push and pop items.
#include <tbb/concurrent_queue.h>

#include <ers/ers.h>

namespace daq {
namespace tm {

const size_t DEF_THR_POOL_SIZE = 24 ; // number of threads in the pool
const size_t MAX_THR_POOL_SIZE = 1024 ; // avoid creating of too many threads via environment
const char NTHREADS_ENVVAR[] = "TDAQ_TMGR_NTHREADS" ; // environment var for redefining N of threads in the pool

class ThreadPool ;

// needed to construct threads like std::thread(WorkerThread(*this))
class WorkerThread {
public:
    WorkerThread(ThreadPool &s) : pool(s) { ERS_DEBUG(2, "Worker thread created" ) ; }
    void operator()() ; // pops a Task from pool and runs it
    ThreadPool &pool ;  // reference to holder pool
};


/**
* \brief Thread pool implementaion, used by TM for asynchronous handling of different events, like PMG callbacks about completed tests.
*    
* Keeps a queue (tbb::concurrent_bounded_queue) of executable tasks and a pool of worker threads that pop and execute the tasks.
* A thread is locked in pop() awaiting for new tasks.
*
* \ingroup public
* \sa
*/
class ThreadPool {
friend class WorkerThread ;

public:

    /**
     * the constructor just launches some amount of workers
     * \param pool_size number of worker threads to create
     * \throws BadConfiguration in case the TDAQ_TMGR_NTHREADS environment can not be parsed or specifies too many threads
    */ 
    ThreadPool(size_t pool_size = DEF_THR_POOL_SIZE) ;

    ~ThreadPool() ;

    // unsafe, use for monitoring only
//    size_t () const { return tasks_queue.size() ; }

    // schedule async task execution: pushes the task F to the queue
    template<typename F>
    void schedule(F) ;

    // schedule async task execution: constructs in place a callable function and pushes it to the queue
    template<typename... Args>
    void schedule(Args&&...) ;

private:
 
    // collection of worker threads so we can join them
    std::vector< std::thread > worker_threads ;

    // the tasks queue
    tbb::concurrent_bounded_queue< std::function<void() > > tasks_queue ;
 
} ;

// add new work item to the queue
// F: anything std::function<void()> can be constructed from, a plenty of options
template<typename F>
void ThreadPool::schedule(F f)
{
    tasks_queue.push(std::move(f)) ; // force to use push(&&) semantic

ERS_DEBUG(3, "Job scheduled") ;
}

// emplaced construction of a callable
template<typename... Args>
void ThreadPool::schedule(Args&&... args)
{
    tasks_queue.emplace(args...);

ERS_DEBUG(3, "Job scheduled") ;
}
}} // daq::tm

#endif
