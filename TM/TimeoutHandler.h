#ifndef TM_TIMEOUTH_H
#define TM_TIMEOUTH_H

#include <chrono>
#include <thread>
#include <memory>
#include <mutex>
#include <atomic>
#include <shared_mutex>
#include <string>
#include <condition_variable>
#include <memory>

// is a class template that represents an unordered associative container.
// It stores key-value pairs with unique keys and supports concurrent insertion, lookup, and traversal, but does not support concurrent erasure.
#include <tbb/concurrent_unordered_map.h>

namespace daq {
namespace tm {

class TestTask ;

/** 
 * \class TimeoutHandler
 * \brief Owns a thread which awakes every second and checks for timeouts in running Tasks.
 * \warning Destrcution of the object may block for up to the wake up period (e.g. 1s)
 * \see daq::tm::Client inherits this functionality.
 * \see daq::tm::TestTask
 *
 */ 
class TimeoutHandler
{
const long s_lapse = 1l ; // seconds

mutable std::shared_mutex m_shrdmtx ; // protect access to nonsafe methods of concurrent_unordered_map
std::mutex m_cndmtx ;
std::condition_variable m_condition ; // mechanism to signale the thread to exit
std::atomic_bool m_to_stop ;
std::thread m_thread ; // periodic thread asynchronously checking for timeouts in Tasks
tbb::concurrent_unordered_map<std::string, std::weak_ptr<daq::tm::TestTask>> m_tasks ; // non-owing collection of all active tasks

void run() ;

protected:
void destroy() ; // called from Client in destrcution

public:
TimeoutHandler() ;
~TimeoutHandler() ;

/**
 * Adds a task with some unique key (e.g. hash of the pointer to c++ object) to the collectoion of active tasks
*/
void addTask(const std::string& key, const std::shared_ptr<daq::tm::TestTask>& task) ;


void removeTask(const std::string& key) ;

} ;

}}

#endif
