#include <unistd.h>
#include <time.h>  
#include <string>
#include <ers/ers.h>
#include <tmgr/tmresult.h>
#include <boost/program_options.hpp>
#include <pmg/pmg_initSync.h>
#include <boost/random.hpp>
#include <random>

const int DEF_DELAY = 1 ;
const int DEF_START_DELAY = 0 ;
const float DEF_FAIL = 0.0 ;

namespace po = boost::program_options;

int main(int argc, const char * const * argv)
{


  int ret = 183;
  bool verbose=false;
  bool randDelay=false;
  int delay=DEF_DELAY;
  int startDelay=DEF_START_DELAY;
  float fail=DEF_FAIL;
   try {
     po::options_description desc("A dummy test returning TM_PASS or TM_FAIL with a configurable probability");
          desc.add_options()
       ("verbose,v"       , po::bool_switch(&verbose)                                 , "Verbose output")
       ("delay,d"         , po::value<int>(&delay)->default_value(delay)                , "Delay before returning result")
       ("startDelay,s"    , po::value<int>(&startDelay)->default_value(startDelay)                , "Delay before issuing PMG sync")
       ("RandomDelay,R"   , po::bool_switch(&randDelay)                          , "Makes the delay d random in [0,d]")
       ("fail,f"          , po::value<float>(&fail)->default_value(fail)                , "Probability of failing the test.")
       ("returnCode,r"    , po::value<int>(&ret)->default_value(ret)                , "Return code to give upon failure.")
       ("help,h"                                                                               , "Print this help message.")
       ;

     po::variables_map vm;
     po::store(po::parse_command_line(argc, argv, desc), vm);
     po::notify(vm);

     if(vm.count("help")) {
       std::cout << desc << std::endl;
       return daq::tmgr::TmUnresolved;
     }
   }
   catch(std::exception& ex) {
     return daq::tmgr::TmUnresolved;
  }
   
   if ( verbose )
	ERS_LOG("Dummy test before running: sleeping for "<<startDelay<<" seconds..." );


   boost::random::mt19937 rng;
   std::random_device rd;
   rng.seed(rd());
   boost::random::uniform_int_distribution<> delayDistr(0,delay);
   boost::random::uniform_real_distribution<float> probDistr(0,1);

   sleep(startDelay) ;
   pmg_initSync();

   if( randDelay ) {
	   delay = delayDistr(rng);
   }

   if ( verbose )
	ERS_LOG("Dummy test running: sleeping for "<<delay<<" seconds..." );

//   std::cout << "Will return with " << ret << " after a delay of " << delay << " secs" << std::endl;
   while(delay) {
     --delay;
     if ( verbose ) 
       ERS_LOG("Dummy test is sleeping...");
     sleep(1);
   }

//   srand( (unsigned)time( NULL ) );
   
//   float uni=(float) rand()/RAND_MAX ;
   float uni = probDistr(rng);
//   std::cout << "uni :" <<  uni << " and fail : " << fail << "\n" << std::endl;

   if ( verbose )
     ERS_LOG("Dummy test FINISHED. Randomly generated: " << uni << " threshold for failing is " << fail );

   if (uni < fail) {
     return ret;
   }

   return daq::tmgr::TmPass ;
}
