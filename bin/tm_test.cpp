#include <mutex>
#include <atomic>
#include <iterator>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <config/Configuration.h>
#include <dal/util.h>
#include <dal/Partition.h>
#include <dal/OnlineSegment.h>
#include <dal/Computer.h>
#include <dal/BaseApplication.h>

#include <TM/Client.h>

namespace po = boost::program_options ;

namespace daq {
ERS_DECLARE_ISSUE(      tm,
                        WrongProgramOptions,
                        "Wrong program options passed: " << text,
                        ((std::string)text )
                )
// to report errors from application
ERS_DECLARE_ISSUE(  tm,
                    Error,
                    message,
                    ((std::string)message)
                    )
}

std::mutex sync_out ;
std::atomic_int counter = 0 ;
bool verbose = false ;

void test_callback(const daq::tm::ComponentResult& res)
{
std::cout << res.component_id << " testing completed, global return code: " << res.result << std::endl ;
counter ++ ;

std::lock_guard<std::mutex> lock(sync_out) ;
   if ( res.result == daq::tmgr::TestResult::TmUntested )
        {
            if ( res.test_results.size() )
                {
                std::cerr << "Failed to launch tests. Issues: " << std::endl ;
                for ( auto tr: res.test_results )
                    {
                        if ( !tr.stderr.empty() )
                            {   
                                std::cerr << tr.test_id << ":" << std::endl ;
                                std::cerr << tr.stderr << std::endl ; }
                    for ( auto issue: tr.issues )
                        { ers::error (*issue) ; } 
                    }
                }
            return ;
        }

    if ( !verbose ) return ;

    std::cout << "Individual test results and output: " << std::endl ;

    for ( auto tr: res.test_results )
        {
        std::cout << "=====================================" << std::endl ;
        std::cout << tr.test_id << ": " << tr.return_code << " (" << tr.duration.count() << "us)"<< std::endl ;
        if ( !tr.diagnosis.empty() )
            {
            std::cout << "Diagnosis: " <<  tr.diagnosis << std::endl ;
            }

        std::cout << "stdout and stderr output: " << std::endl ;
        std::cout << tr.stdout << std::endl ;
        std::cout << tr.stderr << std::endl ;
        std::cout << "-------------------------------------" << std::endl ;
        }

}

int main(int argc, char** argv)
{
    try
	{ IPCCore::init( argc, argv ) ; }
    catch ( daq::ipc::CannotInitialize & ex )
	{
	ers::warning( ex ) ;
	return 1 ;
	}
    catch ( daq::ipc::AlreadyInitialized & ex )
	{ ; }	// just ignore

    std::string database ;
    std::string partition ;
    std::string objectid ;
    std::string classid ;
    std::string singletest ;
    bool pverbose = false ;
    bool tostop = false ;
    size_t repetition = 1 ;

    po::variables_map vm ;
    po::options_description desc("TM test program, allows to test a particular component in a database") ;

	try {
		desc.add_options()
			("database,d",	    po::value<std::string>(&database), 		"database reference, e.g oksconfig:/path/to/filename.xml")
			("partition,p",	    po::value<std::string>(&partition), 	"Partition")
			("objectid,o",	    po::value<std::string>(&objectid), 		"Object ID, must inherit from TestableObject class")
			("classid,c",	    po::value<std::string>(&classid), 		"Test all objects of this class (of TestableObject)")
			("testid,t",	    po::value<std::string>(&singletest), 	"Select single test to launch")
			("verbose,v",	    po::bool_switch(&pverbose)->default_value(false), 		    "Printout test outputs")
			("stop,s",	        po::bool_switch(&tostop)->default_value(false), 		        "Stop tests after launching")
			("repetition,r",	po::value<size_t>(&repetition)->default_value(repetition),	"number of repetitions")
			("help,h", 		"Print help message") ;

		po::store(po::parse_command_line(argc, argv, desc), vm) ;
		po::notify(vm) ;
		}
	catch( std::exception& ex )
		{
		std::cerr << ex.what() << std::endl ;
		return 0 ;
		}

    verbose = pverbose ;

 	if ( vm.count("help") )
 		{
 		std::cout << desc << std::endl ;
 		return 0 ;
 		}

	if ( partition.empty() )
		{
		std::cerr << "partition name neither specified in program options nor defined in TDAQ_PARTITION environment." << std::endl ;
 		std::cout << desc << std::endl ;
		return 1 ;
		}

	if ( classid.empty() && objectid.empty() )
		{
		std::cerr << "Don't know what to test: either ObjectID or ClassID must be specicied." << std::endl ;
 		std::cout << desc << std::endl ;
		return 1 ;
		}

	std::unique_ptr<Configuration> confdb ;	
	try 	{ confdb = std::make_unique<Configuration>(database) ; }
	catch 	( const daq::config::Exception & ex)
		{
		std::ostringstream text ;
		text << "Failed to load configuration database "<< database ;
		ers::error ( daq::tm::Error(ERS_HERE, text.str(), ex) ) ;
        return 2 ;
		}

ERS_LOG("Configuration loaded") ;

    auto dalpartition = daq::core::get_partition(*confdb, partition) ;
    if (dalpartition==nullptr) { std::cerr << "Partition not found in database " << std::endl; return 3; }
    auto substVar = new daq::core::SubstituteVariables(*dalpartition) ;
    dalpartition->get_segment(dalpartition->get_OnlineInfrastructure()->UID()) ;
    confdb->register_converter(substVar); // throw

ERS_LOG("Partition found") ;

    std::unique_ptr<daq::tm::Client>  tmclient ;
    try {
        tmclient = std::make_unique<daq::tm::Client>(*confdb, *dalpartition) ;
    }
    catch (const ers::Issue& ex )
        {
        ers::error (ex) ;
        return 3 ;
        }

ERS_LOG("TM_Client initiaized") ;

    tmclient->setDiscardLogs(false) ;

    using Handle = std::weak_ptr<daq::tm::TestHandle> ;
    Handle* handles ;
    std::vector<const daq::core::TestableObject*> tobjs;
        
    if ( !objectid.empty() )
        {
        const daq::core::TestableObject* to = confdb->get<daq::core::TestableObject>(objectid);
        if ( to == nullptr ) 
            {
            ers::error ( daq::tm::Error(ERS_HERE, "Failed to cast " + objectid + " to TestableObject") ) ;   
            return 4 ;
            }
        tobjs.push_back(to) ;
        auto tlist = tmclient->getTestsForComponent(*to) ;
        std::cout << "The following tests will be launched: " << std::endl ;
        for ( auto t: tlist )
            {
            std::cerr << t->UID() << ":" << std::endl ;
            std::cerr << "\t" << *t << std::endl ;
            }

        handles = new Handle[repetition] ;

        auto start_time = std::chrono::system_clock::now() ; 
  
        for (size_t i=0; i<repetition; i++ )
            {
            try {
                handles[i] = tmclient->testComponent(*to, singletest, test_callback, "any", 0) ;
                }
            catch ( const ers::Issue& ex )
                {
                ers::error(ex) ;
                return 9 ;
                }

            if ( tostop )
                { 
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(300ms);
                std::cout << "Stopping tests" << std::endl ;
                if ( auto handle = handles[i].lock() ) { handle->stop() ; }
                }
            }

            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start_time) ;

            std::cout << repetition << " tests launched, time: " << duration.count() << " microseconds" << std::endl ;

            //std::cerr << "waiting for future result..." << std::endl ;   
        for (size_t i=0; i<repetition; i++ ) {
            if ( auto handle = handles[i].lock() ) {
                daq::tm::ComponentResult res = handle->getResult(); // throw?
                std::cout << "Testing completed, sync result: " << res.result << std::endl ;
    
            }
        }
        
        duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start_time) ;
        std::cout << repetition << "All results received, time: " << duration.count() << " microseconds" << std::endl ;

        } // single object test

    else
        { 
        if ( classid == "Computer" )
            {
            std::vector<const daq::core::Computer*> objects ;
            confdb->get<daq::core::Computer>(objects) ;
            for ( auto c: objects ) tobjs.push_back(c) ;
            }
        else 
        if ( classid == "BaseApplication" )
            {
            std::vector<const daq::core::BaseApplication *> allapps = dalpartition->get_all_applications() ;
            for ( auto c: allapps ) tobjs.push_back(c) ;
            }
        else   
            {
            std::cerr << "Class " << classid << " not supported, use Computer or BaseApplication" << std::endl ;
            return 8 ; 
            } 

        handles = new Handle[tobjs.size()] ;

        auto start_time = std::chrono::system_clock::now() ; 

        size_t i = 0 ;
        for ( auto obj: tobjs )
            {
            std::cerr << "%%%%%%%%%%%\nTesting " << obj->UID() << std::endl ;
            try {
                handles[i] = tmclient->testComponent(*obj, singletest, test_callback) ;
                //daq::tm::ComponentResult res = handles[i]->getResult() ;
                i++ ;
                //std::cout << res.component_id << ": " << res.result << std::endl ;
            }
            catch ( const ers::Issue& ex )
            {
                ers::error(ex) ;
                return 9 ;
            }
            }
            
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start_time) ;
            
std::cerr << "All tests launched in " << duration.count() << " microseconds" << std::endl ;
        if ( tostop )
            {
            for ( size_t i = 0; i < tobjs.size(); i++  )
                { if ( auto handle = handles[i].lock() ) handle->stop() ; }
std::cerr << "All tests stopped" << std::endl ;
            }
        for ( size_t i = 0; i < tobjs.size() ; i++  )
            {
            auto handle = handles[i].lock() ;
            if ( !handle ) continue ;
            daq::tm::ComponentResult res = handle->getResult();
            std::cout << res.component_id << ": " << res.result << std::endl ;
            if ( verbose ) 
                {
                for ( auto tr: res.test_results )
                    {
                    std::cout << "=====================================" << std::endl ;
                    std::cout << tr.test_id << ": " << tr.return_code << " (" << tr.duration.count() << "us)"<< std::endl ;
                    if ( !tr.diagnosis.empty() )
                        {
                        std::cout << "Diagnosis: " <<  tr.diagnosis << std::endl ;
                        }

                    std::cout << "stdout and stderr output: " << std::endl ;
                    std::cout << tr.stdout << std::endl ;
                    std::cout << tr.stderr << std::endl ;
                    std::cout << "-------------------------------------" << std::endl ;
                    }
                }
            } 
            
            duration = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start_time) ;

std::cerr << "All promised results fulfilled in " << duration.count() << " microseconds"  << std::endl ;
std::cerr << "Callbacks completed out of " << tobjs.size() << ": "<< counter << std::endl ;
        }
 // process callbacks
//std::this_thread::sleep_for(std::chrono::seconds(1));
//std::cerr << "After 1s Callbacks completed: " << counter << " out of " << tobjs.size() + repetition - 1 << std::endl ;

while ( counter != tobjs.size() + (objectid.empty() ? 1: repetition) - 1) std::this_thread::yield() ;
std::cerr << "All callbacks completed: " << counter << std::endl ;

delete[] handles ;
std::cerr << "Deleted Handles" << std::endl ;
tmclient.reset() ; // destroy TM client
std::cerr << "Client deleted" << std::endl ;
tobjs.clear() ;
std::cerr << "TObjects deleted" << std::endl ;
return 0;
}