# Test Manager

## tdaq-09-00-00

A complete re-implementation of the former Test Manager package used in Run 2, addressing scalability (threading), performance :muscle: and code maintenance issues. New design was presented here:
https://indico.cern.ch/event/780731/contributions/3250021/attachments/1769325/2880324/DVS_TM_LS2.pdf

:point_up: Schema for describing Tests (Test4Class, Test4Object) did not change, in the schema for TestPolicies there are few changes in TestFailureAction configuration as described here: 
https://indico.cern.ch/event/800655/contributions/3327341/attachments/1801247/2938116/TM_CC_25Feb2019.pdf

Each Failure may have (generic) TestFailureAction associated, and the action and action parameters are the attributes of the TestFailureAction class. There are no more specialized classes like RebootAction, all types of actions (e.g. 'reboot', 'execute', 'test') may be specified as string (enumeration) in TestFailureAction::action attribute, i.e. it is possible to add new actions when needed. The parameters of an action is configured as Json { param1: value1} string in TestFailureAction::parameters attribute (e.g. for 'reboot' action), or taken from associated Executable object (e.g. for 'exec' action). After substitution of parameters (e.g. #this.RunsOn.UID), the action and a json string with parameters are passed as part of TestResult to the client (DVS, RC or CHIP) who is respobsible for execution of the action. An example of an action ('test'):
```
<obj class="TestFailureAction" id="TestExtraComputer">
 <attr name="action" type="enum">"test"</attr>
 <attr name="parameters" type="string">"{ Component: #this.RunsOn.UID; Scope: diagnosis }"</attr>
   ...
</obj>
```
![](../schema/TestRepository.schema.png)

Performance of the new implementation was studied in this presentation:
https://indico.cern.ch/event/817692/contributions/3413542/attachments/1838805/3013693/TMperfCC060519.pdf
