# Test Manager package

### Introduction

Part of the Control and Configuration infrastructure packages.
Organizes execution of Tests for TDAQ components. The Tests are described in configuration according to the [schema](schema/TestRepository.view.pdf) and executed via Process Manager service.

Provides C++ and Java API.
Used by RunControl (via DVS layer) for automatic tests of applications and hosts or by DVS GUI for manual execution of tests for any component.

### Documentation

[twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltTestManager)

### Bugs, discussions

[Test Manager JIRA](https://its.cern.ch/jira/projects/ATDAQCCTM)
