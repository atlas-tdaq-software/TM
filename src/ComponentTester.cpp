#include <algorithm>
#include <chrono>
#include <functional>
#include <numeric>
#include <unordered_set>
#include <utility>

#include <TM/ComponentTester.h>
#include <TM/TestTask.h>
#include <TM/Client.h>
#include <TM/Exceptions.h>

namespace daq {
namespace tm  {


// Create collection (TODO graph) of TestTasks for all Test, launches them (all) asyncronously
ComponentTester::ComponentTester(
    const std::vector<const daq::tm::Test*>& tests,
    const daq::core::TestableObject& object,
    const std::string& onetest,
    Client& client,
    TestResultCallback& callback )
    : m_component(object), m_client(client), m_callback(callback), m_state(false)
{
    // no tests found for this component - return Unsupported
    if ( tests.empty() )
        {
ERS_DEBUG(0, "No tests were found for " << object.UID()) ;
        fulfillResult() ;
        return ;
        }
    
    for ( const auto test: tests )
        {
            if ( !onetest.empty() && test->UID() != onetest ) continue ;
            auto titer = m_tasks.emplace(test->UID(), new TestTask(*test, object.UID() + '@' + object.class_name(), m_client.getConfig())) ; // Tests have unique IDs
ERS_DEBUG(2, "Added Task " << test->UID()) ;
            if ( onetest.empty() && (!test->get_ExecDependsOnSuccess().empty() || !test->get_ExecDependsOnFailure().empty()) )
                {
                    ERS_DEBUG(2, "Task " << test->UID() << " is not ready") ;
                    titer.first->second->setNotReady() ; 
                }
        } 

    try {
        checkDependencies() ; // bad test configuration - return Unsupported 
    } catch ( const daq::tm::Exception &ex ) {
ERS_DEBUG(0, "Configuration of Test dependencies for component " << m_component.UID() << " wrong: " + ex.message()) ;
        fulfillResult(&ex) ;
        throw ex ;
    }

ERS_DEBUG(2, "ComponentTester constructed") ;
}

void ComponentTester::checkDependencies() // throw
{
   for ( auto task: m_tasks )
        {
        for ( auto deptest: task.second->getTest().get_ExecDependsOnSuccess() )
            {
            if ( m_tasks.find(deptest->UID()) == m_tasks.end() )
                throw daq::tm::BadTestConfiguration(ERS_HERE, deptest->UID(), task.first, m_component.UID()) ;
            }
        for ( auto deptest: task.second->getTest().get_ExecDependsOnFailure() )
            {
            if ( m_tasks.find(deptest->UID()) == m_tasks.end() )
                throw daq::tm::BadTestConfiguration(ERS_HERE, deptest->UID(), task.first, m_component.UID()) ;
            }

        std::unordered_set<std::string> visited({task.second->getTest().UID()}) ;
        visitNodes(visited,task.second->getTest().get_ExecDependsOnSuccess()) ;
        visitNodes(visited,task.second->getTest().get_ExecDependsOnFailure()) ;
        }
}

// auxillary function
void ComponentTester::visitNodes(std::unordered_set<std::string>& visited, std::vector<const daq::tm::Test*> children)
{
    for ( auto child : children )
        {
        auto [it, result] = visited.insert(child->UID()) ;
        if ( !result )
            {
            throw CircularTestDependency(ERS_HERE, child->UID()) ;
            }
        visitNodes(visited, child->get_ExecDependsOnFailure()) ;
        visitNodes(visited, child->get_ExecDependsOnSuccess()) ;
        }
}

// called a) upon construction b) upon completion of a TestTask, from PMG callback, to check if a depednent Test is to be launched
// if nothing more to be launched, fulfillResult
void ComponentTester::launchAllReadyTasks() noexcept
{
std::for_each(m_tasks.begin(), m_tasks.end(), [this](auto t){ if (t.second->isReady()) t.second->launch(shared_from_this()); }) ;
ERS_DEBUG(2, "All ready tasks scheduled") ;
}

void ComponentTester::stopTests() noexcept
{
ERS_DEBUG(2, "Stopping all tests") ;
for ( auto t: m_tasks ) // safe, as m_tasks is not changed during lifetime
        {
        t.second->stop() ;
        }
}

ComponentTester::~ComponentTester() 
{
ERS_DEBUG(2, "Destroying tester") ;
std::lock_guard<std::mutex> lock(m_sync_mutex) ;
// for ( auto& task: m_tasks ) { delete task.second ; } Tasks auto-removed as soon as PMG callbacks gets processed and unregistered
m_tasks.clear() ;
ERS_DEBUG(2, "Tester destroyed") ;
}

// called (from PMG callback TestTask::updateStatus)
// when a Tasks completed -> need to check if ExecDependsOn task is ready now
void  ComponentTester::taskCompleted() 
{
ERS_DEBUG(2, "A task completed") ;

    { std::lock_guard<std::mutex> lock(m_sync_mutex) ; // protect from concurrent updates from different TestTasks
    if ( m_state ) return ; // concurrent tasks completed
bool allcompleted(true) ;
bool moretasks(false) ;
for ( auto& task: m_tasks )
    {
    if ( task.second->getStatus() == TestTask::Inprogress )
        {
ERS_DEBUG(2, "Task " << task.second->getTest().UID() << " not completed") ;
        allcompleted = false ;
        } 

    if ( task.second->getStatus() == TestTask::Notready ) // a task waiting for some other to finish
        {
ERS_DEBUG(2, "Task " << task.second->getTest().UID() << " not ready") ;
        for ( auto deptest: task.second->getTest().get_ExecDependsOnSuccess() )
            {
ERS_DEBUG(2, "Checking DependsOnSuccess task status" << deptest->UID()) ;
            auto deptask = m_tasks.find(deptest->UID()) ;
            if ( deptask == m_tasks.end() ) { continue ; } 
            if (    deptask->second->isCompleted() && 
                    deptask->second->getTmResult() == daq::tmgr::TestResult::TmPass ) 
                {
ERS_DEBUG(2, "Task " << task.second->getTest().UID() << " becomes ready: " << deptest->UID() << " passed") ;
                task.second->setReady() ;
                moretasks = true ;
                }
            }
        for ( auto deptest: task.second->getTest().get_ExecDependsOnFailure() )
            {
ERS_DEBUG(2, "Checking DependsOnFailure task status" << deptest->UID()) ;
            auto deptask = m_tasks.find(deptest->UID()) ;
            if ( deptask == m_tasks.end() ) { continue ; } // not applicable for this Test
            if (    deptask->second->isCompleted() && 
                    deptask->second->getTmResult() == daq::tmgr::TestResult::TmFail ) 
                {
ERS_DEBUG(2, "Task " << task.second->getTest().UID() << " becomes ready: " << deptest->UID() << " failed") ;
                task.second->setReady() ;
                moretasks = true ;
                }
            }
        } // not ready check
    } // all tasks

    if ( moretasks )
        {
ERS_DEBUG(2, "Launching more tasks, returning from taskCompleted") ;
        launchAllReadyTasks() ;
        return ;
        }

    if ( !allcompleted )
        {
ERS_DEBUG(2, "Not all tasks completed, returning from taskCompleted") ;
        return ;
        }
    m_state = true ;
    } // protected
// no more callbacks expected, need to collect all results and notify user

ERS_DEBUG(2, "All ready tasks completed, returning") ;
fulfillResult() ;
ERS_DEBUG(2, "Result built") ;
}

//struct ComponentResult {
//        tmgr::TestResult result; // enum { Passed, Undefined, Failed, Unresolved, Untested, Unsupported }
//        std::chrono::time_point<std::chrono::system_clock> test_time;
//        std::vector<TestResult> testResults;    // detailed results of single Tests
//};

// called from (TestTask) callback, when all tests finished and ComponentResult can be built
// in case Issue is provided, it means the result can not be fuillfilled normally (TmUntested) and Issue is attached to the result
// calls user callback, NB: this is happening from CORBA thread
// fillfills the future result
void ComponentTester::fulfillResult(const ers::Issue* ex) noexcept
{
ComponentResult result ;
result.result = daq::tmgr::TestResult::TmUnsupported ; // return this in case no tests were defined
result.component_id = m_component.class_name() + "@" + m_component.UID() ;

if ( ex != nullptr ) { // no TestTasks were launched because of issues in constructor, build a reasonable global result
    TestResult res ;
    res.return_code = daq::tmgr::TestResult::TmUntested;
    res.issues.emplace_back(ex->clone()) ; 
    result.test_results.push_back(res) ;
    result.result = daq::tmgr::TestResult::TmUntested ;
} else {
    if ( !m_tasks.empty() ) {
        for ( auto& task: m_tasks ) // combine results of different Tests for this Component
            {
            if ( !task.second->isCompleted() ) continue ; // ignore Tasks which were not executed
            result.test_results.push_back(task.second->getResult()) ; // keep a copy of Tast result
            }

        result.result = std::accumulate(m_tasks.begin(), m_tasks.end(), daq::tmgr::TestResult::TmPass, 
        [](daq::tmgr::TestResult res, auto tt) { // tasks with Undefined result (not launched) ignored
                            if ( res == daq::tmgr::TestResult::TmUntested || tt.second->getTmResult() == daq::tmgr::TestResult::TmUntested ) return daq::tmgr::TestResult::TmUntested ;
                            if ( res == daq::tmgr::TestResult::TmUnresolved || tt.second->getTmResult() == daq::tmgr::TestResult::TmUnresolved ) return daq::tmgr::TestResult::TmUnresolved ;
                            if ( res == daq::tmgr::TestResult::TmFail || tt.second->getTmResult() == daq::tmgr::TestResult::TmFail ) return daq::tmgr::TestResult::TmFail ;
                            return res ; // Passed
                        } ) ;
        }
}

ERS_DEBUG(2, "ComponentResult is ready. Calling user callback.") ;
m_callback(result) ;    // user callback

try {
    m_result.set_value(result) ; // future fulfilled
    }
catch ( const std::exception& ex ) 
    {
    std::cerr << "Future already fulfilled for component " << m_component.UID() << ": " << ex.what() << std::endl ;
    return ;
    } ;

ERS_DEBUG(2, "Future fulfilled for " << result.component_id) ;
}

}}
