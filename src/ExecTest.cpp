#include <stdio.h>
#include <atomic>
#include <string>
#include <memory> 
#include <tuple>
#include <vector>
#include <map>
#include <functional>

#include <tbb/concurrent_unordered_map.h>

#include <ProcessManager/Exceptions.h>
#include <ProcessManager/ProcessDescription.h>

#include <dal/ComputerProgram.h>
#include <dal/Computer.h>
#include <dal/Partition.h>
#include <dal/util.h>

#include <TM/Exceptions.h>
#include <TM/TestTask.h>
#include <TM/Client.h>

namespace daq {
namespace tm {

// auxilliary local function and structure
// no need to sync, if we only add and access elements in this map
tbb::concurrent_unordered_map<std::string, std::atomic<size_t>> bad_counters ; // counter of error per host
void report_bad_host(const std::string& hostname, const ers::Issue& issue) ;

// nothrow, launched in TP
void TestTask::launchExecTest(const std::string& exectestid, std::weak_ptr<TestTask> taskwp)
{
auto task = taskwp.lock() ;
if ( !task ) { return ; } // protection from being called on destroyed object

auto tester = m_tester.lock() ;
if ( !tester ) { return ; } // all deleted, just exit

ERS_DEBUG(2, "Launching exec test " << exectestid) ;

const daq::core::Computer* host ;
std::string hostname ;
bool user_host = false ; // flag used to re-try on cluster
size_t attempts = 3 ; // number ot attempts to reach PMG
while ( attempts )
    {
    try { // catch re-thrown PMG exceptions
    try { // catch everything, re-throw PMG
        auto exec = std::get<0>(m_exec_tests[exectestid]) ;
        const daq::core::ComputerProgram* program = exec->get_Executes() ;
        const daq::core::Partition& partition = tester->getClient().getPartition() ;
        if ( (hostname=exec->get_Host()).empty() ) // configuration does not specify Host for this test, so select a random one from the pool
            {
            auto host = tester->getClient().get_random_host() ;
            if ( !host ) {
                ERS_LOG("No good hosts left in TestHosts pool, the test won't be launched") ;
                throw NoTestHosts(ERS_HERE, partition.UID()) ;
            }
            hostname = host.value()->UID() ;
    
ERS_DEBUG(3, "Got random host for test: " << hostname) ;

            auto it = bad_counters.find(hostname) ;
            if ( it != bad_counters.end() )
                {
                if ( (*it).second > tester->getClient().getFailuresThreshold() ) // effectively exclude 
                    {
                    ERS_DEBUG(0, "Host " << hostname << " has " << (*it).second << " consecutive communication issues, will not use it.") ;
                    attempts-- ;
                    continue ; // try another host
                    }
                }
        }
        else {
            user_host = true ;
            tester->getClient().substituteParam(tester->getComponent(), hostname) ;
ERS_DEBUG(3, "Test host after subsitution: " << hostname) ;
        }

        std::string exec_params = exec->get_Parameters() ; 
ERS_DEBUG(3, "Test parameters before subsitution: " << exec_params) ;
        tester->getClient().substituteParam(tester->getComponent(), exec_params) ;
ERS_DEBUG(3, "Test parameters after subsitution: " << exec_params) ;

        host = tester->getClient().getConfig().get<daq::core::Computer>(hostname) ;
        if ( host == nullptr )
            { throw daq::tm::InconsistentData(ERS_HERE, "Failed to find Host by name " + hostname + " for Test/Executable " + exectestid, tester->getComponent().UID()) ; }

ERS_DEBUG(2, "Found Computer for test: " << host->UID()) ;

        std::string params = exec_params + " " + program->get_DefaultParameters() ; 
        if ( tester->getClient().isVerbose() ) { params += " -v" ; }

        std::map< std::string, std::string >  environment ;
        std::vector< std::string > program_names ;

        for(const auto & tag : partition.get_DefaultTags()) {
            if( daq::core::is_compatible(*tag, *host, partition) ) {
ERS_DEBUG(3, "Selected compatible Tag " << tag) ; 
                    program->get_info(environment, program_names, partition, *tag, *host) ; // throw
                    break ;
            }
        }

        if( program_names.empty() )
            { throw daq::tm::InconsistentData( ERS_HERE, "The partition's default Tags are not compatible for this test on selected host " + host->UID(), tester->getComponent().UID()) ; }

        daq::pmg::ProcessDescription pdesc( hostname, exectestid, partition.UID(), program_names,
            partition.get_WorkingDirectory(), params, environment, partition.get_LogRoot() + '/' + partition.UID() + "/test_logs", "/dev/null",
            program->get_Needs().empty() ? "" : program->UID(), // RM?
            tester->getClient().isDiscardLogs(),
            exec->get_InitTimeout()) ; // throw (daq::pmg::Not_Initialized)

        std::string programs ;
        for ( auto p: program_names ) { programs.append(p).append(":") ; }  
        { std::lock_guard lock(m_sync_mutex) ;
        m_result.stdout.append("Starting test program ").append(programs).append(" with parameters ").append(params).append(" on host ").append(hostname).append("\n") ;
        } 
        // request to start a process (NB: PMG callback may come before we return back)
        pdesc.start(std::bind(&TestTask::processCallbackAsync, this, std::placeholders::_1, shared_from_this(), std::placeholders::_2)) ; // throw pmg::No_PMG_Server, pmg::Bad_PMG_Server, pmg::Failed_Start

ERS_DEBUG(2, "Test process launched") ;

        if ( !user_host ) {
            auto it = bad_counters.find(hostname) ;
            if ( it != bad_counters.end() )    
                { (*it).second = 0 ; } // host was contacted, so reset the error counter
        }

        return ; // the rest is in PMG callback
    } // function-scope try
    catch ( const daq::pmg::No_PMG_Server& ex ) { // try on another host ? TODO: remove Host from the list?
        if ( user_host )
            { throw ; } // re-throw and do not try on another host
        report_bad_host(hostname, ex) ;
        attempts-- ; // continue, select another host from the pool
    }
    catch ( const daq::pmg::Bad_PMG_Server& ex ) { // try on another host ? TODO: remove Host from the list?
        if ( user_host )
            { throw ; } // re-throw and do not try on another host
        report_bad_host(hostname, ex) ;
        attempts-- ; // continue
    }
    catch ( const ers::Issue& issue ) { // global catch, failed to launch or to get info for a test, fill Result with Issue, Untested
ERS_DEBUG(2, "Failed to launch test process:\n" << issue) ;
        std::lock_guard lock(m_sync_mutex) ;
        m_result.issues.emplace_back(issue.clone()) ;
        break ;
    }
    catch ( const std::exception& ex ) { // global catch, failed to launch or to get info for a test, fill Result with Issue, Untested
        std::lock_guard lock(m_sync_mutex) ;
        m_result.issues.emplace_back(new daq::tm::FailedToLaunch(ERS_HERE, m_test.UID(), hostname, ex)) ;
        break ;
    }
    } // try on PMG exceptions
    catch ( const daq::pmg::Exception& ex ) { // re-thrown exception (2 different handled in one place)
        std::lock_guard lock(m_sync_mutex) ;
        m_result.issues.emplace_back(ex.clone()) ;
        break ;   
    } 
    } // while (tocontinue)
// process not started, no PMG callback expected, need to complete this Task
complete(daq::tmgr::TmUntested) ;
}

// report less verbosely on repeating failed attempts on the same host
void report_bad_host(const std::string& hostname, const ers::Issue& issue)
{
auto it = bad_counters.find(hostname) ;
if ( it != bad_counters.end() ) {
    if ( (*it).second++ == 2 ) { // 2 errors from the same host, report only once
        ers::warning(BadTestHost(ERS_HERE, hostname, issue)) ;
    }
}
else {
    bad_counters.emplace(hostname, 1) ;
}
}

}} // ns
