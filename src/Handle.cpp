#include <TM/Handle.h>
#include <TM/ComponentTester.h>

namespace daq::tm  {

void TestHandle::stop()
{ m_tester->stopTests() ; } 

TestHandle::TestHandle( std::shared_ptr<ComponentTester> tester ) 
: m_tester(tester) {
    m_result = m_tester->m_result.get_future() ; // first initialize future of the result from the promised result in ComponentTester
    m_tester->launchAllReadyTasks() ; // now launch all tests
} 

TestHandle::~TestHandle() { } 

} // namespace