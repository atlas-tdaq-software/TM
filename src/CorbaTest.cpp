#include <stdint.h>
#include <string>

#include <omniORB4/CORBA.h>  
#include <omniORB4/templatedecls.h>

#include <ipc/partition.h>
#include <ipc/ipc.hh>
#include <ipc/policy.h>
#include <ipc/exceptions.h>

#include <ProcessManager/Singleton.h>

#include <TestManager/TestFailureAction.h>
#include <TestManager/TestFailure.h>
#include <TestManager/Test.h>

#include <tmgr/tmresult.h>
#include <TM/Exceptions.h>
#include <TM/TestResult.h>
#include <TM/TestTask.h>
#include <TM/ComponentTester.h>
#include <TM/Client.h>

namespace daq {
namespace tm {

// CORBA test for this type of servant is handled differently in IPC lookup section: 
// call to PMG library is used, which may bypass IPC lookup in initial partition in case
// PMG is configured with a fixed port number, like at P1
const std::string PMG_SERVANT_NAME { "pmgpriv/SERVER" } ;

// nothrow, launched in TP
// may last as long as CORBA timeout
// at the end gets the result and completes TestTask
void TestTask::executeCORBATest(std::weak_ptr<TestTask> taskwp)
{
auto task = taskwp.lock() ;
if ( !task ) return ; // protection from being called on destroyed object

auto tester = m_tester.lock() ;
if ( !tester ) { return ; } // all deleted, just exit

uint32_t result = daq::tmgr::TmUndef ;

try {
    std::string servant_type = m_corba_test->get_ServantType();
    std::string cpartition_name = m_corba_test->get_CPartitionName();
    std::string servant_name = m_corba_test->get_ServantName();

    tester->getClient().substituteParam(tester->getComponent(), servant_name);
    tester->getClient().substituteParam(tester->getComponent(), servant_type);

ERS_DEBUG(2, "CORBA test: "<<servant_name << ":"<<servant_type << " in partition " << cpartition_name);

    // why is that? it's more explicit to say #this.UID in the Test4CORBAServant instance
    if( servant_name.empty() ) {
        servant_name = tester->getComponent().UID();
    }

    if( servant_type.empty() ) {
        throw daq::tm::InconsistentData( ERS_HERE, "IPC InterfaceName not defined for component " + servant_name, tester->getComponent().UID()) ;
    }

    ::ipc::servant_var servant ;

// get PMG server CORBA reference via PMG client call
// see https://gitlab.cern.ch/atlas-tdaq-software/ProcessManager/-/blob/master/src/lib/Singleton.cxx#L785

    if ( servant_type == PMG_SERVANT_NAME ) {
        try {
ERS_DEBUG(2, "Using pmg::Singleton for getting corba ref to " + servant_name);
            pmgpriv::SERVER_var pmg_ptr = daq::pmg::Singleton::instance()->get_server(servant_name) ;
            servant = pmgpriv::SERVER::_duplicate(pmg_ptr.in()) ; // e.g. "AGENT_pc-tdq-onl-01.cern.ch"
ERS_DEBUG(3, "Got SERVANT corba ref to " + servant_name);
        }
        catch ( const pmg::No_PMG_Server& ex ) {
            m_result.stderr = ex.what() ;
            result = daq::tmgr::TmFail ;
        }
    } else { // other type of CORBA interfaces, get the ref from IPC
        try {
            IPCPartition partition(cpartition_name) ;
            servant = partition.lookup<::ipc::servant, ::ipc::no_cache, ::ipc::non_existent>(servant_name, servant_type) ;
ERS_DEBUG(2, "IPC lookup passed") ;
        }
        catch (const daq::ipc::InvalidPartition & e) {
            m_result.stderr = e.what() ;
            result = daq::tmgr::TmUnresolved ;	// we did not get the ref because some external infrastrcuture problem, so we can not judge
ERS_DEBUG(2, "InvalidPartition: " << e) ;
        }
        catch (const ipc::ObjectNotFound& e) { // lookup failed: no publication in IPC, the rest is OK => test failed
            m_result.stderr = e.what() ;
            result = daq::tmgr::TmFail ;
ERS_DEBUG(2, "Object " << servant_name << ":" << servant_type << " not registered in IPC: " << e) ;
        }
        catch (const ipc::Exception& e) { // lookup failed: no partition server, no global IPC, no publication in IPC
            m_result.stderr = e.what() ;
            result = daq::tmgr::TmUnresolved ; // Unresiolved or Failed? what to tell about servant if IPC infrastrcuture failed?
ERS_DEBUG(2, "CORBA test Failed in IPC: " << e);
        }
        catch( const CORBA::Exception& e ) { // any other issue when talking to (existing & registered) object, it is in bad shape -> Failed 
            m_result.stderr = "Failed in CORBA communication " + std::string(e._name()) ;
ERS_DEBUG(2, "IPC resolve failed in CORBA communication: " << e._name());
            result = daq::tmgr::TmUnresolved ;
        }
    } // ifnot PMG server

    // now we have ref to the object, call IDL test() method implementation on it 
    try {
        if ( m_corba_test->get_Timeout() != 0 )
            { 
            omniORB::setClientCallTimeout(servant, m_corba_test->get_Timeout()*1000) ; // milliseconds
            }
        servant->test(m_test.UID().c_str()) ;
ERS_DEBUG(2, "CORBA test passed");
        result = daq::tmgr::TmPass ;
    }
    catch(const ::ipc::TestFailed& e) { // object contacted but returned Failed test result (remote)
        result = e.error ;
        std::ostringstream msg;
        msg << "CORBA Test called on object and failed:\n" << e.what ;
        m_result.stderr = msg.str();
        }
    catch(CORBA::SystemException& e) { // CORBA-specific exception, wrapped in IPC
        daq::ipc::CorbaSystemException ipcex{ ERS_HERE, &e } ;
        m_result.stderr = "Failed to communicate with CORBA object: " + ipcex.message() ;
        result = daq::tmgr::TmFail;
    }
    catch(const CORBA::Exception& e) { // any other issue when talking to (existing & registered) object, it is in bad shape -> Failed 
        m_result.stderr = "User exception thrown when communicating with CORBA object: " + std::string(e._name()) ;
        result = daq::tmgr::TmFail;
    }
} // big try 
catch (const ers::Issue& ex)
    {
ERS_DEBUG(2, "CORBA test Untested: " << ex);
    result = daq::tmgr::TmUntested ; 
    m_result.issues.emplace_back(ex.clone()) ; // store exception
    }
catch (...)
    {
    m_result.stderr = "Unexpected exception ... in CORBA test" ;
    std::cerr << m_result.stderr << std::endl ;
    result = daq::tmgr::TmUntested ;
    }

for ( const auto failure: m_test.get_Failures() ) // check if a TestFailure matches to received exit code
    {
    if ( failure->get_ReturnCode() == result )
        {
        for ( const auto action: failure->get_Actions() )
            {
            m_result.actions.push_back(action) ;
            }
        m_result.diagnosis.append(failure->get_diagnosis()) ;
        m_result.diagnosis.append("\n") ;
        }
    }

TestTask::complete((daq::tmgr::TestResult)result) ;

ERS_DEBUG(2, "CORBA test task completed: " << m_result.return_code);
}
}} //namespace
