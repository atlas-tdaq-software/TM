#include <memory>
#include <mutex>
#include <tuple> 
#include <algorithm>
#include <iostream> 

#include <ers/ers.h>
#include <ProcessManager/defs.h>
#include <pmgpriv/pmgpriv.hh>
#include <pmgpub/pmgpub.hh>

#include <TestManager/TestFailure.h>

#include <TM/TestTask.h>
#include <TM/Client.h>

namespace daq {
namespace tm {

// just pushes the real call into a thread pull execution queue
// called from CORBA thread pool (pmg) when a process changes its state
void TestTask::processCallbackAsync(process_sp_t process, std::weak_ptr<TestTask> taskwp, void*)
{    
    auto tester = m_tester.lock() ;
    if ( !tester ) { return ; } // all deleted, just exit

    tester->getClient().schedule(std::bind(&TestTask::processCallback, this, process, taskwp) ) ; // async exec in TP
}

// called from CORBA thread pool (pmg) when a process changes its state
// 
void TestTask::processCallback(process_sp_t process, std::weak_ptr<TestTask> taskwp) 
{
auto task = taskwp.lock() ; // if not yet deleted, create a shared_ptr to the currently used Task
if ( !task ) { return ; } ; // protect from late callbacks coming after deletion of the Task

auto tester = m_tester.lock() ;
if ( !tester ) { return ; } // all deleted, just exit

ERS_DEBUG(2, "Got PMG callback for process " << process->name() << " state: " <<  process->status().state) ;

{ std::lock_guard lock(m_sync_mutex) ;
if ( m_status == Killed || m_status == Completed ) {
ERS_DEBUG(2, "Late callback, Task is already completed: " << m_status << ": " << process->name() << ":" << process->status().state) ;
    return ; }
} // protecttion from late callbacks when Task is done already

auto testlookup = m_exec_tests.find(process->name()) ;
if ( testlookup == m_exec_tests.end() ) {
    if ( process->status().state == pmgpub::ProcessState::RUNNING ) { // late callback, Task is already completed and probably destroyed?
        ERS_DEBUG(0, "Late RUNNING callback, Process " << process->name() << " is already completed and removed from the list") ;
        return ;
    }
    ERS_LOG("Unexpected callback for a process that is not found in map: process " << process->name() << " in state " << process->status().state << std::endl) ;
    // ERS_ASSERT_(testlookup != m_exec_tests.end()) ;   
} ;

auto state = process->status().state ;
const auto& pinfo = process->status().info ;

{ std::lock_guard lock(m_sync_mutex) ; 
  std::get<1>(testlookup->second) = process ; } // store the Process pointer, the launched bool is already set to true

switch ( state )
    {
    case pmgpub::ProcessState::RUNNING:
ERS_DEBUG(2,"Test process " << process->name() << " RUNNING, so try to launch dependednt processes") ;
        launchRemainingExecs() ; // some dependent Execs may be waiting for this one
    return ; // nothing to be done here, will be called again later with terminal state

    case pmgpub::ProcessState::NOTAV:
    case pmgpub::ProcessState::REQUESTED:
    case pmgpub::ProcessState::LAUNCHING:
    case pmgpub::ProcessState::CREATED:
    return ;  // will be called again later with terminal state, or may be we don't get such states in callback at all?

    case pmgpub::ProcessState::SIGNALED: // e.g. killed (15,9) on timeout; segfaulted etc
ERS_DEBUG(2, "Test process " << process->name() << " SIGNALED:" << std::to_string(pinfo.signal_value)) ;
        { std::lock_guard lock(m_sync_mutex) ; m_result.stderr += "Test process was signaled: " + std::to_string(pinfo.signal_value) + "\n"; }
        complete(daq::tmgr::TestResult::TmUntested) ; 

    case pmgpub::ProcessState::FAILED:
    case pmgpub::ProcessState::SYNCERROR:
ERS_DEBUG(2, "Test process " << process->name() << " failed to start: " << process->status().failure_str_hr ) ;
        { std::lock_guard lock(m_sync_mutex) ;
          m_result.stderr.append("Test process failed to start: ").append(process->status().failure_str_hr).append("\n") ; }
        complete(daq::tmgr::TestResult::TmUntested) ;

    case pmgpub::ProcessState::EXITED:
    break ;

    default: // not possible
ERS_LOG("Unexpected state of process " << process->name() << ": " << state << ", please report if you see this...") ;
    ;
    }

// process finished (exited), returning either of EXITED SIGNALED SYNCERROR FAILED

ERS_DEBUG(2,"Test process finished, checking other Execs") ;

for ( auto& astest: m_exec_tests ) {
ERS_DEBUG(6, "Exec " << astest.first) ;
    if ( !std::get<2>(astest.second) ) { // not all Execs launched .. are we ever here? yes we can be here if EXITED comes before RUNNING
ERS_DEBUG(2, "Not all Execs yet started for this Test") ;
        launchRemainingExecs() ;
        return ;
    }
    auto process = std::get<1>(astest.second) ;
    if ( !process ) { return ; } // test process launched but not yet called back
    if ( !process->exited() ) { // test process still running ...
ERS_DEBUG(2,"Not all Execs yet finished for this Test") ;
        return ;
    }
}

// all Execs finished, may determine common result
// { lock() ; no lock needed as there may be no other Execs for this Test?
m_result.return_code = daq::tmgr::TestResult::TmPass ;

ERS_DEBUG(2, "All Execs finished for this Test, can build the result") ;
{ std::lock_guard lock(m_sync_mutex) ; 
for ( auto& astest: m_exec_tests ) {
    auto process = std::get<1>(astest.second) ;
    const auto& pinfo = process->status().info ; //   process_id signal_value exit_value

    switch ( process->status().state ) // from good to bad, ovewrite: passed->failed->unresolved->notdone
        {
        case pmgpub::ProcessState::EXITED:
ERS_DEBUG(2, "Test process normally finished " << astest.first << " :" << pinfo.exit_value) ;
            if ( pinfo.exit_value ) // failed or unresolved (183), Passed=0 otherwise
                {
                if ( m_result.return_code != daq::tmgr::TmUntested && m_result.return_code != daq::tmgr::TmUnresolved ) // highest priority
                     { m_result.return_code = (pinfo.exit_value == daq::tmgr::TmUnresolved ? daq::tmgr::TmUnresolved : daq::tmgr::TmFail) ; }
                
                for ( const auto failure: m_test.get_Failures() ) // check if a TestFailure matches to received exit code
                    {
                    if ( failure->get_ReturnCode() == (u_int64_t)pinfo.exit_value )
                        {
                            for ( const auto action: failure->get_Actions() )
                                {
                                m_result.actions.push_back(action) ;
                                }
                            m_result.diagnosis.append(failure->get_diagnosis()) ;
                            m_result.diagnosis.append("\n") ;
                        }
                    }
                }
            try { // get the output
                if ( process->status().out_is_available )
                    {
                    pmgpriv::File_var outLog ;
                    process->outFile(outLog) ;
                    m_result.stdout += (char*)outLog->get_buffer() ;
                    }
                if ( process->status().err_is_available )
                    {
                    pmgpriv::File_var outLog ;
                    process->errFile(outLog);
                    m_result.stderr += (char*)outLog->get_buffer() ;
                    }
                }
            catch(const pmg::Exception& ex)
                {
                m_result.stderr += "Failed to retreive test output from host:\n" ;
                m_result.stderr += ex.what() ;
                }

            if ( pinfo.exit_value == 127 ) // libraries or program not found. TODO: wrong command line options?
                {
                m_result.stderr += "Failed to execute test program (binary or library not found, exit code 127)\n" ;

ERS_DEBUG(1, "Failed to execute binary for the test: " << m_result.stdout << '\n' << m_result.stderr) ;
                m_result.return_code = daq::tmgr::TestResult::TmUntested ;
                }
        break ;

        default: ;
            // don't care
        }
    m_result.stderr += "\n";
    process->unlink() ; // do it here, as soon as we don't need Process info
    // std::get<1>(astest.second) = nullptr ; // prevent timeout handler to operate on this unlinked process
}
} // end of protected area

ERS_DEBUG(2, "Task completed for test " << m_test.UID() << " with result: " << m_result.return_code) ;

complete(m_result.return_code) ;

if ( m_result.return_code == daq::tmgr::TestResult::TmPass ) {
ERS_DEBUG(2, "Caching Passed test result for " << tester->getComponent().UID() << "@" << m_test.UID()) ;
    tester->getClient().cacheResult(tester->getComponent().UID(), m_test.UID(), m_result) ;
}
}

}}
