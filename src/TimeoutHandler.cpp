#include <functional>
#include <vector>

#include <TM/TestTask.h>
#include <TM/TimeoutHandler.h>

namespace daq {
namespace tm {


void TimeoutHandler::run()
{   
    while (true)
    {
        {  // the way to sleep for 1sec and to exit thread when necessary
        std::unique_lock lk(m_cndmtx);
        if ( m_condition.wait_for(lk, std::chrono::seconds(s_lapse), [this]{ return m_to_stop == true ; } ) )
            { 
ERS_DEBUG(2, "Exiting timeout handler thread") ;
            return ;
            } 
        }

    // create another collection of tasks ready to be stopped to avoid deadlock in checkTimeout->stop->complete->removeTask
    std::vector<std::weak_ptr<daq::tm::TestTask>> tasks_2stop ;

    { std::shared_lock lock(m_shrdmtx) ;
      for ( auto wtask: m_tasks ) {
	    if ( auto task = wtask.second.lock() ) // get shared_ptr
        	{
		    if ( task->checkTimeout() ) // does not call anything
	            {
ERS_DEBUG(2, "Task " << wtask.first << " to be terminated by timeout") ;
            	    tasks_2stop.emplace_back(wtask.second) ;
            	    }
		}
        }
    }

    for ( auto twp: tasks_2stop ) {
        if ( auto task = twp.lock() ) {
            task->stop(true) ;
            ERS_DEBUG(2, "Task " << task->getTest().UID() << " stopped") ;
            }
        }
    } // while forever
} 

TimeoutHandler::TimeoutHandler():
m_shrdmtx(), m_cndmtx(), m_condition(), m_to_stop(false), m_thread(std::bind(&TimeoutHandler::run, this))
{} 

TimeoutHandler::~TimeoutHandler()
{}

void TimeoutHandler::destroy()
{
    m_to_stop = true ;
    m_condition.notify_one() ;  // notify thread
    m_thread.join() ;           // and wait for completion
    std::unique_lock lock(m_shrdmtx) ;
    m_tasks.clear() ;
ERS_DEBUG(2, "Timeout handler destroyed") ;
} 

void
TimeoutHandler::addTask(const std::string& key, const std::shared_ptr<daq::tm::TestTask>& task)
{
    std::shared_lock lock(m_shrdmtx);
    auto [iter, result] = m_tasks.emplace(key, task) ; // creates weak_ptr to TestTask from task: no need to own it

ERS_DEBUG(3, "number of Tasks in the map: " << m_tasks.size() ) ;

    // TODO this is for debugging
/*    if ( result == false ) {
        std::cerr << "Bad state: failed to register timeout for Task " << key << std::endl ;
        std::cerr << "Dump of the m_tasks: " << std::endl ;
        for ( auto t: m_tasks )
            {
            std::cerr << t.first << std::endl ;
            }
    } */
    
// ERS_ASSERT_MSG(result == true, "Failed to register timeout for Task " << key) ;
ERS_DEBUG(3, "Timeout handler added for " << key) ;
}

void
TimeoutHandler::removeTask(const std::string& key)
{
    std::unique_lock lock(m_shrdmtx);
ERS_DEBUG(3, "Removed task from timeout handler: " << key) ;
    m_tasks.unsafe_erase(key) ;
}
}}
