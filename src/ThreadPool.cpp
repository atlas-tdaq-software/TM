#include <stdlib.h>
#include <string>
#include <exception>
#include <functional> 
#include <algorithm> 
#include <mutex>  
	
#include <ers/ers.h>
#include "TM/ThreadPool.h"


namespace daq {
ERS_DECLARE_ISSUE(tm,
                       BadConfiguration,
                       message,
                       ((std::string)message) )
}

using namespace daq::tm ;

void WorkerThread::operator()()
{
    decltype(pool.tasks_queue)::value_type atask;
    while(true) {
		try {
			pool.tasks_queue.pop(atask) ; // if there is something in the queue
			ERS_DEBUG(4, "Job is being executed") ;
			atask() ; 			// execute a task, continue
			ERS_DEBUG(4, "Job finished") ;
		}  catch ( const oneapi::tbb::user_abort& ex ) {
			ERS_DEBUG(3, "Exiting from worker thread") ;
			return ;
		}
	}
}

// the constructor just launches some amount of workers
ThreadPool::ThreadPool(size_t nthreads)
{
try {
	if ( const char * var = getenv(NTHREADS_ENVVAR) ) { 
		nthreads = std::stoi(std::string(var)); 
		if ( nthreads > MAX_THR_POOL_SIZE ) {
			throw daq::tm::BadConfiguration(ERS_HERE, "Too many threads requested via TDAQ_TMGR_NTHREADS") ;
		}
	}
} catch ( const std::exception& ex ) {
	throw daq::tm::BadConfiguration(ERS_HERE, "Failed to parse TDAQ_TMGR_NTHREADS environment to integer", ex) ; 
}

worker_threads.reserve(nthreads) ;

for(size_t i = 0; i < nthreads; ++i)
	{ worker_threads.push_back(std::thread(WorkerThread(*this))) ; }
	
ERS_DEBUG(0, "ThreadPool constructed: " << worker_threads.size() << " worker threads created" ) ;
}
 
// the destructor joins all threads
ThreadPool::~ThreadPool()
{
	decltype(tasks_queue)::value_type v ;
	while ( tasks_queue.try_pop(v) ) ; // previous safe implementation of clear

	// stop all threads, by ensuring that each thread gets one and only one task which throws
 	std::for_each(worker_threads.begin(), worker_threads.end(), [&](auto& t) { tasks_queue.push([]() { throw oneapi::tbb::user_abort() ; } ) ; } ) ;
	
	// join them
  	std::for_each(worker_threads.begin(), worker_threads.end(), [](auto& t) { ERS_DEBUG(4, "Waiting for worker " << t.get_id() ) ; t.join() ; } ) ;

ERS_DEBUG(0, "ThreadPool destructred, no more working threads" ) ;
}
