#include <chrono>
#include <ratio>
#include <string>
#include <unordered_set>
#include <functional> 

#include <ers/ers.h>
#include <config/DalObject.h>
#include <ProcessManager/Exceptions.h>

#include <TestManager/Test4CORBAServant.h>
#include <TestManager/ExecutableTest.h>
#include <TestManager/Executable.h>

#include <tmgr/tmresult.h>
#include <TM/Exceptions.h>
#include <TM/TestTask.h>
#include <TM/Client.h>

namespace daq {
    namespace tm {

void checkCircles(std::unordered_set<std::string>& visited, const daq::tm::Executable* exec) ;

// only creates and fills in m_tests map, the real start is done from launch()
TestTask::TestTask(const daq::tm::Test& test, const std::string& objuid, Configuration& config )
: m_tester(), m_test(test), m_result(), m_status(Ready), m_corba_test(nullptr)
{
m_result.test_id = test.UID() ;
if ( auto exectest = config.cast<daq::tm::ExecutableTest>(&test) )
    {
    for ( auto exec: exectest->get_Runs() )
        {
        std::unordered_set<std::string> visited ;
        checkCircles(visited, exec) ; // throw 
        m_exec_tests.emplace(objuid + ":" + test.UID() + ":" + exec->UID(), std::tuple(exec, nullptr, false) ) ;
        }
    }
else
    {
    m_corba_test = config.cast<daq::tm::Test4CORBAServant>(&test) ;
    std::string message = "Failed to cast test " + test.UID() + " to Test4CORBAServant" ;
    if ( m_corba_test == nullptr ) { throw InconsistentData(ERS_HERE, message, test.UID() ) ; }
    // ERS_ASSERT_MSG( m_corba_test != nullptr, "Failed to cast test " << test.UID() << " to Test4CORBAServant" ) ;
    }
}

void checkCircles(std::unordered_set<std::string>& visited, const daq::tm::Executable* exec)
{
    auto [it, result] = visited.insert(exec->UID()) ;
    if ( !result ) 
        { throw CircularTestDependency(ERS_HERE, exec->UID()) ; }

    for ( auto dep: exec->get_InitDependsOn() )
        { checkCircles(visited, dep) ; }
}

// destructor: clean up, stop and unlink processes
// shall we wait for processes to be finished? at the moment not
TestTask::~TestTask()
{
std::lock_guard lock(m_sync_mutex) ; // sync with callbacks?
m_status = Killed ;
m_exec_tests.clear() ;
ERS_DEBUG(2, "Task destroyed: " << m_test.UID()) ;
}

// terminates all running tests (used in timeout)
// returns false if there were issues in stopping the RUNNING processes, so cleanup may be needed
void TestTask::stop(bool tocomplete) noexcept
{
ERS_DEBUG(2, "Stopping Test " << m_test.UID()) ;

bool success = false ;
{ std::lock_guard lock(m_sync_mutex) ; // sync with callbacks, m_status and Execs Process* are thread unsafe
  if ( m_status != Inprogress ) { return ; } }
  
for ( auto& astest: m_exec_tests ) {
ERS_DEBUG(2, "Stopping Process " << astest.first) ;
    std::shared_ptr<daq::pmg::Process> pr ;
    { std::lock_guard lock(m_sync_mutex) ; pr = std::get<1>(astest.second) ; }
    if ( pr && pr->status().state == pmgpub::ProcessState::RUNNING ) {
        try {
ERS_DEBUG(0, "Terminating RUNNING test Exec " << std::get<0>(astest.second)->UID() << " of Task " << m_test.UID()) ;
            pr->kill_soft(3) ; // TODO make configurable?
ERS_DEBUG(0, "Process killed, waiting for callback") ;
            success = true ;
        } catch(const pmg::Process_Already_Exited& ex) { // fine, there will be usual callback
ERS_DEBUG(0, "Process " << std::get<0>(astest.second)->UID() << " already exited") ;
        } catch(const ers::Issue& ex) { // failed to kill process, may be callback won't come - need to clean up what is possible
ERS_DEBUG(0, "Issue in stopping the process: " << ex) ;
            { std::lock_guard lock(m_sync_mutex) ; m_result.issues.emplace_back(ex.clone()) ; }
        }
    } else {
ERS_DEBUG(0, "Attempt to stop non-running or non-existing process for task " << astest.first) ;
        // completed() to be called, zomby task
    }
}

// failed to cleanly kill a process: in case of Timeout need to complete the task, otherwise testing never finishes
// in case kill_soft was OK, we'll get a callback which completes the task. What if not...?
if ( (!success) && tocomplete )  
    {
ERS_DEBUG(0, "Failed to stop an active TestTask " << success << ", need to complete...") ;
    complete(daq::tmgr::TmUntested) ;
    }

ERS_DEBUG(2, "Task processes stopped: " << success) ;
return ;
}

// common code to end the task: unregister in TimeoutHandler, store duration etc
void TestTask::complete(uint32_t res) noexcept
{
ERS_DEBUG(2, "Task completed: " << m_test.UID()) ;
    {
    std::lock_guard lock(m_sync_mutex) ;
    m_status = Completed ;
    m_result.return_code = res ;
    m_end_time = std::chrono::system_clock::now() ; 
    m_result.test_time = std::chrono::system_clock::now() ;
    m_result.duration = std::chrono::duration_cast<std::chrono::microseconds>(m_end_time - m_start_time) ;
    } 

    auto tester = m_tester.lock() ;
    if ( !tester ) { return ; } // all deleted, just exit

    if ( m_test.get_Timeout() ) // unregister this task in timeout handler
        {
        tester->getClient().removeTask(std::to_string(std::hash<daq::tm::TestTask*>()(this)) + tester->getComponent().UID() + m_test.UID()) ;
        }
        
    tester->taskCompleted() ; // signal to upper level to check if other Task can be started now
}

// check the elapsed time and return true if need to be stopped
// called from timeout handler thread, need to sync
// return true if timeout occured and tests processes are to be stopped (from TimeoutHandler thread)
// false otherwise
bool TestTask::checkTimeout()
{
 { std::lock_guard lock(m_sync_mutex) ; 
    if ( m_status != TestTask::Inprogress ) { return false ;} }

if ( m_corba_test != nullptr ) { return false ; } // CORBA timeout is handled inside the test
auto duration = (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - m_start_time)).count() ;
ERS_DEBUG(2, "Current duration of task " << m_test.UID() << " " << duration << "(us)" ) ;
if ( m_test.get_Timeout()*std::micro::den <= duration )
    {
ERS_DEBUG(0, "Timeout " << m_test.get_Timeout() <<  "(s) triggered for task " << m_test.UID()) ;
    std::ostringstream msg ;
    msg << "Test terminated by timeout (" << m_test.get_Timeout() << "s)\n" ;
    { std::lock_guard lock(m_sync_mutex) ; m_result.stderr.append(msg.str()) ; }
    return true ;
    }
ERS_DEBUG(3, "Task " << m_test.UID() << " is still running, no timeout" ) ;
return false ;
}

// called once from ComponentTester, no need to sync
void TestTask::launch( std::shared_ptr<ComponentTester> tester ) noexcept
{
m_tester = tester ;
m_status = Inprogress ;
m_start_time = std::chrono::system_clock::now() ; 

const auto cached_test_result = tester->getClient().getCachedResult(tester->getComponent().UID(), m_test) ;
if ( cached_test_result )
    {
ERS_DEBUG(2, "Result for " << tester->getComponent().UID() << "@" << m_test.UID() << " retreived from cache: " << m_result.return_code) ;
    m_result = *cached_test_result ;
    m_result.stdout.append("++ CACHED RESULT ++\n") ;

    // must call complete() asyncrounously to avoid circular and thus blocking call to launch()
    tester->getClient().schedule(std::bind(&TestTask::complete, this, m_result.return_code ) ) ; // async exec in TP
    return ;
    }

ERS_DEBUG(2, "Result not cached, launching new task " << m_test.UID()) ;

if ( nullptr != m_corba_test )
    {
    std::function<void()>&& ff = std::bind(&TestTask::executeCORBATest, this, shared_from_this()) ;
    tester->getClient().schedule(std::move(ff)) ;
    ERS_DEBUG(2, "CORBA test scheduled") ;
    return ; // only one CORBA test is possible, nothing else to do, the rest is done in executeCORBATest
    }

if ( m_test.get_Timeout() ) // register this task in timeout handler
    {
    tester->getClient().addTask(std::to_string(std::hash<daq::tm::TestTask*>()(this)) + tester->getComponent().UID() + m_test.UID(), shared_from_this()) ;
    }

launchRemainingExecs() ; // start executable tests processes
}

// called first from launch() and then from processCallback, launches Executables which are ready (e.g. when dependedt one is launched)
// in turn calls corresponding ComponentTester code to launch next Task if needed
// do no block, all blocking calls are asynced in TP

void TestTask::launchRemainingExecs() noexcept
{
auto tester = m_tester.lock() ;
if ( !tester ) { return ; } // all deleted, just exit

ERS_DEBUG(2, "Launching remaining exec tests") ;
std::lock_guard lock(m_sync_mutex) ;
for ( auto& [testid, exect]: m_exec_tests )
    {
    if ( std::get<2>(exect).load() ) { ERS_DEBUG(2, "Already launched") ; continue ; } // atomic check if this Exec is already launched
   
    auto& exec = std::get<0>(exect) ;   // throw ?
    for ( const auto& depexec: exec->get_InitDependsOn() ) // check if dependent processes started
        {
        const std::string deptestid = m_test.UID() + ":" + depexec->UID() ;
ERS_DEBUG(2, "Checking status of dependent Exec: " << deptestid) ;
        std::shared_ptr<daq::pmg::Process> pr = std::get<1>(m_exec_tests[deptestid]) ; 
        if ( !pr ) { goto nextexec ; } // do not start this ExecTest: dependent Exec not yet launched
        if ( pr->status().state != pmgpub::ProcessState::RUNNING && pr->status().state != pmgpub::ProcessState::EXITED )
            {
ERS_DEBUG(2, "Dependent Exec: " << depexec->UID() << " is not yet ready: " << pr->status().state) ;   
            goto nextexec ; 
            } // do not start this ExecTest: dependent Exec not yet running
        }

    if ( !std::get<2>(exect).exchange(true) )  // lockless, atomic check and set
        {
            std::function<void()>&& ff = std::bind(&TestTask::launchExecTest, this, testid, shared_from_this()) ;
            tester->getClient().schedule(std::move(ff)) ; // async exec in TP
ERS_DEBUG(2, "Scheduled TestTask Exec " + testid ) ;
        } 

    nextexec: ;
    }
}

}} // daq::tm
