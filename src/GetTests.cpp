#include <vector>
#include <string>
#include <algorithm>
#include <stddef.h>

#include <ers/ers.h>
#include <config/Schema.h>
#include <config/DalObject.h>

#include <TestManager/Test4Object.h>
#include <TestManager/Test4Class.h>
#include <TestManager/Test4CORBAServant.h>

#include <TM/Client.h>

namespace daq{
namespace tm {    

std::vector<const daq::tm::Test*> 
Client::getTestsForComponent(const daq::core::TestableObject& comp, std::string scope, int level) noexcept
{
    std::vector<std::string> classes = m_config.get_class_info(comp.class_name(), false).p_superclasses ;
    classes.push_back(comp.class_name());
ERS_DEBUG(3, "Getting tests for component " << comp.UID() << "@" << comp.class_name()) ;
#ifndef ERS_NO_DEBUG
ERS_DEBUG(3, "Hierarchy of classes for " << comp.UID() << "@" << comp.class_name() << ":") ;
for ( auto c: classes )
    {
ERS_DEBUG(3,c) ;
    }
#endif

    std::vector<const daq::tm::Test*> ret;
    bool skip_test4class = false;
    // get Test4Objects
ERS_DEBUG(3, "Getting tests4object") ;

    for(const auto & a : m_all_tests) {
        //filter scope and level
ERS_DEBUG(4, "Checking test "<< a->UID()) ;
        if((a->get_Complexity() > level)
                || ((scope != "any")
                        && (std::find(a->get_Scope().begin(), a->get_Scope().end(), "any") == a->get_Scope().end())
                        && (std::find(a->get_Scope().begin(), a->get_Scope().end(), scope) == a->get_Scope().end()))) {
ERS_DEBUG(4, "Scope does not match, skipping") ;
            continue;
        }

        //Cast to special tests
        if(const daq::tm::Test4Object * tfc = m_config.cast<daq::tm::Test4Object>(a)) {
            if(tfc->get_Interactive() == false) {
                for(const auto & o : tfc->get_Objects()) {
ERS_DEBUG(4, "Test " << a->UID() << " defined for object "<< o->UID()) ;
                    if(o->UID() == comp.UID()) {
ERS_DEBUG(4, "Test " << a->UID() << " matches, added to the list") ;
                        ret.push_back(a);
                        if(tfc->get_OverrideTest4Class() == true) { //std::cout << "clean the list " <<std::endl;
                            skip_test4class = true;
                        }
                        break;
                    }
                }
            }
        }
    }

    //now get other tests if appropriate
    if(!skip_test4class) {
ERS_DEBUG(4, "Getting from test4class") ;
        for(auto a : m_all_tests) {
ERS_DEBUG(4, "Checking test "<< a->UID()) ;
            //filter scope and level
            if((a->get_Complexity() > level)
                    || ((scope != "any")
                            && (std::find(a->get_Scope().begin(), a->get_Scope().end(), "any") == a->get_Scope().end())
                            && (std::find(a->get_Scope().begin(), a->get_Scope().end(), scope) == a->get_Scope().end()))) {
ERS_DEBUG(4, "Scope does not match, skipping") ;
                continue;
            }

            //Cast to special tests class tests
            if(const daq::tm::Test4CORBAServant * tfc = m_config.cast<daq::tm::Test4CORBAServant>(a)) {
ERS_DEBUG(4, "Test Test4CORBAServant@" << a->UID() << " has " << tfc->get_ClassName().size() << " objects in the 'Classes' relationship") ;
                for(size_t j = 0; j < tfc->get_ClassName().size(); ++j) {
ERS_DEBUG(4, "Test Test4CORBAServant@" << a->UID() << " is defined for class " << tfc->get_ClassName()[j]) ;
                    if(std::find(classes.begin(), classes.end(), tfc->get_ClassName()[j]) != classes.end()) {
ERS_DEBUG(3, "Test Test4CORBAServant@" << a->UID() << " selected for object "<< comp.UID() << "@" << comp.class_name()) ;
                        ret.push_back(a);
                    }
                }
            } else if(const daq::tm::Test4Class * tfc = m_config.cast<daq::tm::Test4Class>(a)) {
                if(tfc->get_Interactive() == false) {
                    if(std::find(classes.begin(), classes.end(), tfc->get_ClassName()) != classes.end()) {
ERS_DEBUG(3, "Test for class " << tfc->get_ClassName() << " matches class or superclass of "<< comp.UID() << "@" << comp.class_name()) ;
                        ret.push_back(a);
                    }
                }
            }
        }
    }
#ifndef ERS_NO_DEBUG
    ERS_DEBUG(1, "Relevant tests for " << comp.UID() << " are:"); 
    for(const auto& test: ret ) {
        ERS_DEBUG(1, *test);
    }
#endif
    return ret ;
}

}}
