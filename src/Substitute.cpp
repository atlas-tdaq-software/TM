#include <stddef.h> 
#include <stdint.h>
#include <algorithm>
#include <ostream>
#include <string> 
#include <vector>

#include <ers/ers.h>

#include <config/Schema.h>
#include <config/ConfigObject.h>
#include <config/DalObject.h>
#include <config/Errors.h>

#include <TM/Exceptions.h>
#include <TM/Client.h>

namespace daq {
namespace tm {

void Client::substituteParam(const DalObject& obj, std::string& params, bool forceObj) const
{
ERS_DEBUG(4, "Substitution reqested for " << params) ;

    size_t il = params.find('#'), ir;
    while(il != std::string::npos) {
        if((ir = params.find_first_of(" ;\"", il + 1)) == std::string::npos)
            ir = params.size();
        std::string keyword = params.substr(il, ir - il); // now keyword is #keyword.token1.token2.attrib

ERS_DEBUG(4, "Keyword found: " << keyword) ;
        if(keyword.find("#this.") != std::string::npos) {

            const DalObject* currobj = &obj;
            size_t kil = keyword.find('.'); // keyword index left
            size_t kir = kil;               // ---		 right
            size_t tokenCounter = 0;
            do {
                ++tokenCounter;

                kir = keyword.find('.', kil + 1); // can be at the end of the keyword
                std::string token = keyword.substr(kil + 1, kir - kil - 1);

                // token that is between "."s can be:
                //	- attribute of the current object: Value substituted.
                //	- relationship to other object: Then object is replaced and cycle continues
                //	- reverse relationship, starting with "&": the same
                //	- "UID" keyword, meaning UID of the current object

                ERS_DEBUG(4, "Resolving token " << token << " for object " << currobj->UID());
                // need to be substituted to the DB ID of the object
                if(token == "UID") {
                    params.replace(il, ir - il, currobj->UID());
                    break;
                } else if(token == "ClassName") { // need to be substituted to the DB Class Name of the object
                    params.replace(il, ir - il, currobj->class_name());
                    break;
                } else if(kir == std::string::npos) {
                    std::string s_value; std::ostringstream value;
                    uint64_t u64_value; int64_t s64_value; uint32_t u32_value; int32_t s32_value; int16_t u16_value;
                    uint16_t s16_value; int8_t s8_value; uint8_t u8_value;
                    ConfigObject& config_obj = const_cast<ConfigObject&>(currobj->config_object()) ;

                try {
			        for( const auto& att: m_config.get_class_info(currobj->class_name(), false ).p_attributes )
                        {
                        if( att.p_name != token ) continue; // look for particular attribbute amoung all attributes of the class

                        switch ( att.p_type )
                            {
                            	case daq::config::string_type: 
                                case daq::config::enum_type:
                                    config_obj.get(token, s_value);
                                    value.str(s_value);
                                break ;

                                case daq::config::u64_type:
                                    config_obj.get(token, u64_value);
                                    value << u64_value;
                                break ;

                                case daq::config::s64_type:
                                    config_obj.get(token, s64_value);
                                    value << s64_value;
                                break ;

                                case daq::config::u32_type:
                                    config_obj.get(token, u32_value);
                                    value << u32_value;
                                break ;

                                case daq::config::s32_type:
                                    config_obj.get(token, s32_value);
                                    value << s32_value;
                                break ;

                                case daq::config::u16_type:
                                    config_obj.get(token, u16_value);
                                    value << u16_value;
                                break ;

                                case daq::config::s16_type:
                                    config_obj.get(token, s16_value);
                                    value << s16_value;
                                break ;

                                case daq::config::s8_type:
                                    config_obj.get(token, s8_value);
                                    value << static_cast<short>(s8_value);
                                break ;

                                case daq::config::u8_type:
                                    config_obj.get(token, u8_value);
                                    value << static_cast<unsigned short>(u8_value);
                                break ;

                                default:
                                    std::ostringstream text;
                                    text << "The type of attribute '" << token << "' in the test template parameter '"
                                        << keyword << "' is not supported by testDAL.";
                                throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID() );
				            } // switch
                            params.replace(il, ir - il, value.str());
                            break; // do not look for other attributes
                        } // interate over all attributes
                    } // try
                    catch(daq::config::Exception & ex) {
                        std::ostringstream text;
                        text << "Could not read meta-information for class " << currobj->class_name() << "\n:" << ex
                                << std::endl;
                        throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID(), ex );
                    }
                    break; // we found attribute, no need to go further
                } // if last token
                  // reverse relationship from another object
                  // should have a form of &ClassName@RelationshipName
                else if(token[0] == '&') {
                    size_t amp = token.find('@', 1);
                    if(amp == std::string::npos) {
                        std::ostringstream text;
                        text << "Wrong template syntax: " << token << " in the parameters of the test for object "
                                << obj.UID();
                        throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID() );
                    };
 
                    // std::string classname = token.substr(1, amp-1) ;
                    std::string relname = token.substr(amp + 1);
                    std::vector<const DalObject*> holders;

                    try {
                        holders = m_config.referenced_by(*currobj, relname, false, false);
                    }
                    catch(daq::config::Exception & ex) {
                        std::ostringstream text;
                        text << "Can not get reverse relationship defined by template syntax: " << token
                                << "in the parameters of the test for object " << obj.UID();
                        throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID(), ex );
                    };

                    if(holders.size() != 1) {
                        std::ostringstream text;
                        text << "Wrong number of objects (" << holders.size()
                                << ") returned for relationship defined by template syntax: " << token
                                << "in the parameters of the test for object " << obj.UID();
                        throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID() );
                    };

                    currobj = holders[0]; // continue with the referenced object
                }
                // token should be a reference to another object
                else {
                    const DalObject* nextobj;
                    std::vector<const DalObject*> refobjs ;
                    if(token == "RunsOn" && forceObj == false) {
                        refobjs = currobj->get("get_host()") ;
                        if ( refobjs.size() == 1 )
                            { nextobj = refobjs[0] ; }
                        else
                            { throw daq::tm::InconsistentData( ERS_HERE, "Can not get host for RunsOn test relationship", obj.UID() ); } // shall never happen
                    } else
                    if(token == "BackupHosts" && forceObj == false) {
                        refobjs = currobj->get("get_backup_hosts()");

                        std::string this_param = params.substr(il, ir - il); // This is, for instance, #this.BackupHosts.UID
                        this_param.erase(this_param.find(token), token.length()); // This is #this..UID
                        this_param.erase(std::find(this_param.begin(), this_param.end(), '.')); // This is #this.UID

                        std::string full;
                        for(const auto c : refobjs) {
                            std::string p = this_param; // copy it
                            substituteParam(*c, p, true);
                            full += p + " ";
                        }
                        full.erase(full.find_last_not_of(" ") + 1);

                        params.replace(il, ir - il, full);

                        break;
                    } else { // single object by relationship
                        try {
        ERS_DEBUG(4, "Looking for objects following " << token << " from object " << currobj->UID()) ;
                            refobjs = currobj->get(token) ;
                                // ir - end of keyworkds
                                // std::string keyword = params.substr(il, ir - il); // now keyword is #keyword.token1.token2.attrib e.g. il->#this.get_host().UID<-ir
                            std::string remaining = "#this" ;
                            remaining.append(keyword.substr(keyword.find(token)+token.length())) ;  // if token1 gives a vector of referenced objects
                                                                                        // iterate over them passing #this.token2.attrib as input
ERS_DEBUG(5, "Substituting parameters " << remaining << " for vector of "<< token) ;
                            std::string result ;
                            for ( auto nextobj: refobjs )
                                {
                                if ( result.size() ) { result.append(" ") ; }
                                auto toreplace = remaining ; // copy and modify
                                substituteParam(*nextobj, toreplace , true) ; // modify
ERS_DEBUG(5, "Got " << toreplace << " for object "<< nextobj->UID()) ;

                                result.append(toreplace) ; // build space-separated list of results for each obj in vector
                                }
                            params.replace(il, ir - il, result);
ERS_DEBUG(5, "Parameters " << params << " replaced with " << result) ;
                            break ; // no further recursion, all done in cycle above, full  #keyword.token1.token2.attrib processed                       
                        } // more severe, can not get reference
                        catch(daq::config::Exception & ex) {
                            std::ostringstream text;
                            text << "Can not resolve reference '" << token << "' in the template parameter '" << keyword
                                    << "' of the test for object " << obj.UID();
                            throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID(), ex );
                        }
                    }
                    
                    currobj = nextobj; // continue with the referenced object
                } // the token is a reference
                kil = kir;
            } while(kir != std::string::npos); // iterating through #this.IsPartOf.BelongsTo.Name
        } // #this. keyword
        else {
            std::ostringstream text;
            text << "Unrecognized syntax '" << keyword << "' in the template parameters of the test for object "
                    << obj.UID();
            throw daq::tm::InconsistentData( ERS_HERE, text.str(), obj.UID() );
        }

        il = params.find('#', il + 1); // next template parameter
    }
}

}}
