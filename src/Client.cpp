#include <stdlib.h>
#include <random>
#include <algorithm>
#include <string>
#include <vector>
#include <memory> 
#include <iterator>

#include <ers/ers.h>
#include <system/Host.h>
#include <config/Configuration.h>
#include <config/DalObject.h>
#include <config/Errors.h>
#include <dal/Partition.h>
#include <dal/OnlineSegment.h>
#include <dal/ComputerSet.h>
#include <dal/Computer.h>

#include <ProcessManager/Singleton.h>
#include <ProcessManager/Exceptions.h>

#include <TestManager/TestSet.h>

#include <TM/Handle.h>
#include <TM/Exceptions.h>
#include <TM/Client.h>
#include <TM/ComponentTester.h>
#include <TM/ThreadPool.h>

namespace daq {
namespace tm  {

void Client::empty_callback(const ComponentResult&) {}

// implementation
Client::Client (Configuration& config, const daq::core::Partition& partition) 
:m_config(config),
m_partition(partition),
m_thread_pool(new ThreadPool())
{

try {
    daq::pmg::Singleton::init();
    }
catch(daq::pmg::Already_Init& ex) { }

add_tests<daq::core::Partition>(&m_partition);
add_tests<daq::core::OnlineSegment>(m_partition.get_OnlineInfrastructure()) ;

const daq::core::ComputerSet* testhosts = m_partition.get_OnlineInfrastructure()->get_TestHosts();
// if ( testhosts == nullptr ) throw daq::tm::NoTestHosts(ERS_HERE) ;
if ( testhosts )
    {
ERS_DEBUG(0, "Getting test hosts from ComputerSet " << testhosts->UID()) ;
    get_test_hosts(*testhosts) ;
    }
 
if( m_test_hosts.empty() ) // if TestHosts not defined for a Partition, use DefaultHost and finally localhost
    {
ERS_DEBUG(0, "No TestHosts defined for partititon, adding default host") ;
    if( m_partition.get_DefaultHost()) {
        m_test_hosts.push_back(m_partition.get_DefaultHost());
        }
    }

const System::LocalHost *thishost = System::LocalHost::instance(); // the localhost here
if( auto lh = m_config.get<daq::core::Computer>(thishost->full_name()) )
    { m_test_hosts.push_back(lh) ; }

if( m_test_hosts.empty() ) {
    throw daq::tm::NoTestHosts(ERS_HERE, m_partition.UID());
}

if ( const char * var = getenv(TDAQ_TMGR_FAILURES_THRSHLD_ENVVAR) )
		{
        m_failures_threshold = std::stoi(std::string(var)) ;
        ERS_DEBUG(0, "Test Host failure threshould set to " << m_failures_threshold) ;
        }
        
ERS_DEBUG(2, "Client constructed") ;
}

Client::~Client() 
{
destroy() ; // early destruction of TimeoutHandler which otherwise keeps pointers to TestTasks
m_handles.clear() ;
delete m_thread_pool ;
ERS_DEBUG(0, "Client destructed") ; 
} 

// return to client non-owning pointer to Handle (and Tester) 
// in order to avoid client to syncronize deletion of Handles and TM Client object
std::weak_ptr<TestHandle>
Client::testComponent(  const daq::core::TestableObject& comp, const std::string test, 
                        TestResultCallback callback, std::string scope, int level)
{
std::shared_ptr<ComponentTester> tester( new ComponentTester(getTestsForComponent(comp, scope, level), comp, test, *this, callback)) ;

// need to create Handle (holding the future of the promise) before launching any task
std::shared_ptr<TestHandle> handle( new TestHandle(tester) ) ; 

// tester->launchAllReadyTasks() ; // here test processes (or corba calls) are started, callback may be called, result may be fulfilled

// keeps handles per Component. Previous handle for the same Component is deleted here.
m_handles[comp.UID()] = handle ;

ERS_DEBUG(2, "ComponentTester launched, handle returned") ;

return std::weak_ptr<TestHandle>(handle) ; // may already contain fulfilled future result
}


void Client::reset(const daq::core::TestableObject& comp)
{
    resetCachedResult(comp.UID(), getTestsForComponent(comp)) ;
}


template<class T> void
Client::add_tests(const T* container)
{
    for( const auto& rep: container->get_TestRepositories() ) {
        const daq::tm::TestSet * ts = m_config.cast<daq::tm::TestSet>(rep);
        if(ts) {
            m_all_tests.insert(ts->get_tests().begin(), ts->get_tests().end());
        }
    }
    
    for( const auto& seg: container->get_Segments() ) {
        add_tests<daq::core::Segment>(seg);
    }
}

void
Client::get_test_hosts(const daq::core::ComputerSet& cs)
{
   for( const auto comp: cs.get_Contains() ) {
        const daq::core::Computer* c = m_config.cast<daq::core::Computer>(comp);
        if((c != nullptr) && (c->get_State() == true)) {
ERS_DEBUG(0, "Host " + c->UID() + " added to the tests hosts pool" ) ;
            m_test_hosts.push_back(c);
        } else if ( const daq::core::ComputerSet* cset = m_config.cast<daq::core::ComputerSet>(comp) ) {
            get_test_hosts(*cset) ;
        }
    }
}

const std::optional<const daq::core::Computer*>
Client::get_random_host() const 
{
    std::vector<const daq::core::Computer*> good_hosts ; // select only hosts with failures counter <= m_failures_threshold
    std::copy_if(m_test_hosts.begin(), m_test_hosts.end(), std::back_inserter<>(good_hosts), [this](auto host) {
        auto it = m_bad_counters.find(host->UID()) ;
        if ( it != m_bad_counters.end() ) {
            return (*it).second <= m_failures_threshold ;
        }
        return true ;
        } ) ;
    if ( good_hosts.empty() ) { return std::nullopt ; }
    std::vector<const daq::core::Computer*> ret ;
    std::sample(good_hosts.begin(), good_hosts.end(), std::back_inserter<>(ret), 1, std::mt19937{std::random_device{}()}) ;
    return std::make_optional(ret.front()) ;
}
}} // daq::tm
