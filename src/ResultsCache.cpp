#include <string>
#include <tuple>
#include <utility>
#include <chrono>

#include <tbb/concurrent_hash_map.h>

#include <ers/ers.h>

#include <TM/ResultsCache.h>

namespace daq::tm {

void ResultsCache::cacheResult(const std::string& comp_id, const std::string& test_id, const TestResult& res)  // key = objectId@testId
{
ERS_DEBUG(3, "Result " << res.return_code << "  cached for " << build_id(comp_id, test_id)) ;

tbb::concurrent_hash_map<std::string, TestResult>::accessor acc ;
m_cache.insert(acc, build_id(comp_id, test_id)) ; // TBB allows concurrent insertion and traversal
acc->second = res ;
}

std::optional<TestResult>
ResultsCache::getCachedResult(const std::string& comp_id, const daq::tm::Test& test) 
{
tbb::concurrent_hash_map<std::string, TestResult>::const_accessor value ;
if ( test.get_Validity() && m_cache.find( value, build_id(comp_id, test.UID()) ) )
    {
ERS_DEBUG(3, "Cached result retreived for " << build_id(comp_id, test.UID())) ;
    auto result_age_secs = (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - value->second.test_time)).count() ;

    if ( result_age_secs <= test.get_Validity() )
        {
ERS_DEBUG(3, "Cached result still valid " << build_id(comp_id, test.UID())) ;
        return std::optional<TestResult>(value->second) ;
        }
    else
        {
ERS_DEBUG(3, "Cached result expired: " << result_age_secs << " seconds of age, test validity: " << test.get_Validity() ) ;
        }
    }
else
    {
ERS_DEBUG(3, "Cached result not found for " << build_id(comp_id, test.UID()) ) ;
    }      
return std::optional<TestResult>() ;
}

void ResultsCache::resetCachedResult(const std::string& comp_id, const std::vector<const daq::tm::Test*>& tests)
{
for (auto t: tests)
    {
    tbb::concurrent_hash_map<std::string, TestResult>::accessor value ;
    if ( m_cache.find(value, build_id(comp_id, t->UID())) )
        {
        m_cache.erase(value) ;
ERS_DEBUG(3, "Cached result cleared for " << build_id(comp_id, t->UID()) ) ;
        }
    }
}

}
